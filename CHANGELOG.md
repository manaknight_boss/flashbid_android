# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

MAJOR version when you make incompatible API changes,

MINOR version when you add functionality in a backwards-compatible manner, and

PATCH version when you make backwards-compatible bug fixes.

## [Unreleased]

## [0.2.0] - 2018-12-11
### Added
- Draw menu
- firebase syncing on draw menu
- edit profile
- s3 upload
- User QR Code
- About link out
- Logout
- History
- Tab menu for home screen
- remove x from all modals
- maintanence mode 
- no internet check

## [0.1.0] - 2018-12-11
### Added
- Login with facebook
- Setup Android
- Terms and condition
- Device ID
- Setup firebase
- Setup ios project
- Reset password
- Login screen
- Register screen
- Forgot password screen