package com.flashbid.flashcoinapp.sharedpreference;

import android.app.WallpaperColors;
import android.content.Context;
import android.content.SharedPreferences;

import com.flashbid.flashcoinapp.R;


public class SharedPreferenceUtility {

    private final SharedPreferences mSharedPreferences;
    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    private static final String MY_USER_ID = "MY_USER_ID";
    private static final String FIREBASE_TOKEN = "FIREBASE_TOKEN";
    private static final String QR_CODE = "QR_CODE";
    private static final String USER_WALLET_BALANCE = "wallet_balance";

    public SharedPreferenceUtility(Context context) {
        mSharedPreferences = context.getSharedPreferences(context.getString(R.string.shared_preference_file),
                Context.MODE_PRIVATE);
    }

    public void setAccessToken(String token) {
        mSharedPreferences.edit()
                .putString(ACCESS_TOKEN, token)
                .apply();
    }

    public String getAccessToken() {
        return mSharedPreferences.getString(ACCESS_TOKEN, null);
    }

    public void setMyUserId(int userId) {
        mSharedPreferences.edit()
                .putInt(MY_USER_ID, userId)
                .apply();
    }

    public int getMyUserId() {
        return mSharedPreferences.getInt(MY_USER_ID, 0);
    }


    public String getFirebaseToken() {
        return mSharedPreferences.getString(FIREBASE_TOKEN, "");
    }

    public void setFirebaseToken(String firebaseToken) {
        mSharedPreferences.edit()
                .putString(FIREBASE_TOKEN, firebaseToken)
                .apply();
    }

    public String getQrCode() {
        return mSharedPreferences.getString(QR_CODE, "");
    }

    public void setQrCode(String qrCode) {
        mSharedPreferences.edit()
                .putString(QR_CODE, qrCode)
                .apply();
    }

    public double getUserWalletBalance() {
        return mSharedPreferences.getFloat(USER_WALLET_BALANCE, 0.0f);
    }

    public void setUserWalletBalance(double balance) {
        mSharedPreferences.edit()
                .putFloat(USER_WALLET_BALANCE, (float)balance)
                .apply();
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }
}
