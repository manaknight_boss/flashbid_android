package com.flashbid.flashcoinapp.networks;


import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.ApiResponse;
import com.flashbid.flashcoinapp.models.Contests;
import com.flashbid.flashcoinapp.models.FlashPay;
import com.flashbid.flashcoinapp.models.History;
import com.flashbid.flashcoinapp.models.Loyalty;
import com.flashbid.flashcoinapp.models.RegisterUser;
import com.flashbid.flashcoinapp.models.Sticker;
import com.flashbid.flashcoinapp.models.TokenResponse;
import com.flashbid.flashcoinapp.models.UserDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiEndpointInterface {

    @POST("register")
    Call<TokenResponse> register(@Body RegisterUser registerUser);

    @POST("login")
    Call<TokenResponse> login(@Body RegisterUser registerUser);

    @POST("forgot")
    Call<TokenResponse> forgotPassword(@Body RegisterUser registerUser);

    @POST("reset")
    Call<TokenResponse> resetPassword(@Body RegisterUser registerUser);

    @POST("login/oauth/facebook")
    Call<TokenResponse> facebookLogin(@Body RegisterUser registerUser);

    @POST("query")
    Call<TokenResponse> sendDeviceId(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<UserDetail> getProfileInfo(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<TokenResponse> updateProfileDetail(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<ApiResponse> logout(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<History> getHistory(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<Loyalty> getLoyalty(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<Contests> getItemDetail(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<Sticker> getStickerDetail(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<FlashPay> getFlashPayDetail(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<ApiResponse> getTicket(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("query")
    Call<ApiResponse> putLike(@Header("Authorization") String authorization, @Body ApiRequest apiRequest);

    @POST("buy")
    Call<ApiResponse> buyItem(@Header("Authorization") String authorization, @Body Contests contest);

    @POST("sticker")
    Call<ApiResponse> buySticker(@Header("Authorization") String authorization, @Body Sticker sticker);

    @POST("pay")
    Call<ApiResponse> payBill(@Header("Authorization") String authorization, @Body FlashPay flashPay);

}
