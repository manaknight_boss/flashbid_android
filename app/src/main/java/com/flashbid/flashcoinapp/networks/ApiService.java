package com.flashbid.flashcoinapp.networks;

import android.util.Log;

import com.flashbid.flashcoinapp.BuildConfig;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private static Retrofit client = null;

    public ApiService() {

    }

    public static ApiEndpointInterface instance() {
        if (client == null) {
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
            client = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .build();
            Log.d("Base_URL", BuildConfig.BASE_URL);
        }

        return client.create(ApiEndpointInterface.class);
    }
}
