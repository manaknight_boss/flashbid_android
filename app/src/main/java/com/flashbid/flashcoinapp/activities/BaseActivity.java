package com.flashbid.flashcoinapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;

import com.amazonaws.util.Base32;
import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.interfaces.PermissionRequestCallBacks;
import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.Device;
import com.flashbid.flashcoinapp.models.TokenResponse;
import com.flashbid.flashcoinapp.models.UserDetail;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.ImageCache;
import com.flashbid.flashcoinapp.utils.ProgressBarDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity {
    protected SharedPreferenceUtility mSharedPreferenceUtility;
    protected Context mContext;
    protected ProgressBarDialog progressDialog;
    DatabaseReference mDatabase;
    private PermissionRequestCallBacks mPermissionRequestListener;
    private static final String IS_FIRST_TIME_PERMISSION_REQUEST = "is_first_time_permission_request";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        progressDialog = new ProgressBarDialog(mContext);
        mDatabase = FirebaseDatabase.getInstance().getReference();
//        changeLocaleForLanguage();
        generateAppToken();
    }

    public void generateAppToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        try {
                            if (task.getResult() != null) {
                                String token = task.getResult().getToken();
                                mSharedPreferenceUtility.setFirebaseToken(token);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
    }

    protected void changeLocaleForLanguage() {
        Locale locale = new Locale("es");
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(
                config,
                getResources().getDisplayMetrics()
        );
    }


    protected void openStartActivity() {
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);
        finish();
    }

    protected void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    protected void openTermsAndConditionActivity() {
        Intent intent = new Intent(mContext, TermsAndConditionActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.no_anim);
    }

    public List<String> getNotGrantedPermissions(String... permissions) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return null;
        }

        List<String> deniedPermissions = new ArrayList<>(permissions.length);

        for (String permission : permissions) {
            int status = ContextCompat.checkSelfPermission(this, permission);
            if (status != PackageManager.PERMISSION_GRANTED) {
                deniedPermissions.add(permission);
            }
        }
        return deniedPermissions;
    }

    public void checkPermissionForMarshMellow(PermissionRequestCallBacks permissionRequestCallBacks, String message, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            this.mPermissionRequestListener = permissionRequestCallBacks;
            boolean isRequestPermissionRationale = true;

            final List<String> notGrantedPermissions = getNotGrantedPermissions(permissions);
            if (notGrantedPermissions.size() == 0) {
                permissionRequestCallBacks.onPermissionsGranted();
                return;
            }
            for (String permission : notGrantedPermissions) {
                if (isRequestPermissionRationale) {
                    isRequestPermissionRationale = ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, permission);
                }
            }

            final SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            final boolean isFirstTime = preferences.getBoolean(IS_FIRST_TIME_PERMISSION_REQUEST, true);
            if (isRequestPermissionRationale || isFirstTime) {
                if (isFirstTime) {
                    preferences.edit().putBoolean(IS_FIRST_TIME_PERMISSION_REQUEST, false).apply();
                }
                String[] onlyDeniedPermissions = getArrayFromList(notGrantedPermissions);
                ActivityCompat.requestPermissions(BaseActivity.this, onlyDeniedPermissions, onlyDeniedPermissions.length);

            } else {
                if (!TextUtils.isEmpty(message)) {
                    DialogUtility.displayPermissionMessageDialog(mContext,message);
                } else {
                    mPermissionRequestListener.onPermissionsDenied();
                }
            }
        } else {
            permissionRequestCallBacks.onPermissionsGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        final List<String> notGrantedPermissions = getNotGrantedPermissions(permissions);
        if (notGrantedPermissions.size() == 0) {
            mPermissionRequestListener.onPermissionsGranted();
        } else {
            mPermissionRequestListener.onPermissionsDenied();
        }
    }

    public static String[] getArrayFromList(List<String> list) {

        if (list == null || list.size() <= 0) {
            return null;
        }

        String[] array = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public Bitmap getBitmapFromImagePath(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        return BitmapFactory.decodeFile(filePath,options);
    }

    public String writeBitmapToLocalPath(Bitmap scaledBitmap, int percentage) {
        String filename = getFilename();
        try {
            FileOutputStream out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, percentage, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filename;
    }

    public String getFilename() {
        ImageCache imageCache = new ImageCache(this);
        File file = imageCache.getCacheDirectory();
        return file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg";
    }

    public void hideKeyboard(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(((Activity) context).getWindow()
                    .getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showNoInternetErrorMessage() {
        DialogUtility.showDialog(mContext,getString(R.string.no_internet_available),Constants.ERROR);
    }

    public void getProfileInfo(final boolean isStartUpCall) {
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setParameter(Constants.NO_PARAMETERS);
        apiRequest.setAction(Constants.ActionName.PROFILE);
        ArrayList<String> requestParameters = new ArrayList<>();
        requestParameters.add("qr_code");
        apiRequest.setResponse(requestParameters);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getProfileInfo(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(@NonNull Call<UserDetail> call, @NonNull Response<UserDetail> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    UserDetail userDetailResponse = response.body();
                    if(userDetailResponse != null) {
                        if (userDetailResponse.getStatusCode() == 403) {
                            DialogUtility.showDialog(mContext, userDetailResponse.getMessage(), Constants.ERROR);
                        } else {
                            String qrCode = userDetailResponse.getQrCode();
                            String encodedQRCode = Base32.encodeAsString(qrCode.getBytes());
                            mSharedPreferenceUtility.setQrCode(encodedQRCode);
                            if(isStartUpCall) {
                                sendDeviceIdToServer();
                            }
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserDetail> call, @NonNull Throwable t) {
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    public void sendDeviceIdToServer() {
        progressDialog.showProgress();
        Device device = new Device();
        device.setDeviceId(mSharedPreferenceUtility.getFirebaseToken());
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setParameter(new Gson().toJson(device));
        apiRequest.setAction(Constants.ActionName.DEVICE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.sendDeviceId(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call, @NonNull Response<TokenResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    TokenResponse tokenResponse = response.body();
                    if (tokenResponse != null && tokenResponse.getStatusCode() == 403) {
                        DialogUtility.showDialog(mContext, tokenResponse.getMessage(), Constants.ERROR);
                    } else {
                        openMainActivity();
                        finish();
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
                progressDialog.hideProgress();
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call, @NonNull Throwable t) {
                progressDialog.hideProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

}
