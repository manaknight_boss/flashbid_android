package com.flashbid.flashcoinapp.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.ActivitySplashBinding;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class SplashActivity extends BaseActivity {

    private static final long SPLASH_TIME_OUT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(mSharedPreferenceUtility.getAccessToken())) {
                    openStartActivity();
                } else {
                    openMainActivity();
                }
            }
        }, TimeUnit.SECONDS.toMillis(SPLASH_TIME_OUT));
    }

}
