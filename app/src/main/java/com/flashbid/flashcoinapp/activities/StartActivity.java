package com.flashbid.flashcoinapp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.ActivityStartBinding;
import com.flashbid.flashcoinapp.databinding.BottomSheetForgotPasswordBinding;
import com.flashbid.flashcoinapp.databinding.BottomSheetLoginBinding;
import com.flashbid.flashcoinapp.databinding.BottomSheetPasswordResetBinding;
import com.flashbid.flashcoinapp.databinding.BottomSheetSignUpBinding;
import com.flashbid.flashcoinapp.interfaces.DialogActionListener;
import com.flashbid.flashcoinapp.models.RegisterUser;
import com.flashbid.flashcoinapp.models.TokenResponse;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.Utils;

import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartActivity extends BaseActivity {
    private ActivityStartBinding mBinding;
    private BottomSheetDialog forgotPasswordDialog;
    private BottomSheetDialog loginDialog;
    private BottomSheetDialog passwordResetDialog;
    private BottomSheetDialog signUpDialog;
    private StartActivity mContext;
    protected CallbackManager callbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferenceUtility = new SharedPreferenceUtility(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_start);
        mContext = this;
        mBinding.termsAndConditionsText.setPaintFlags(mBinding.termsAndConditionsText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLoginDialog();
            }
        });

        mBinding.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSignUpDialog();
            }
        });

        mBinding.accessWithFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isNetworkAvailable(mContext)) {
                    initializeFacebookLogin();
                } else {
                    showNoInternetErrorMessage();
                }
            }
        });
        mBinding.termsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openTermsAndConditionActivity();
            }
        });
    }

    private void openSignUpDialog() {
        final BottomSheetSignUpBinding signUpBinding = BottomSheetSignUpBinding.inflate(LayoutInflater.from(mContext));
        View view = signUpBinding.getRoot();
        signUpDialog = new BottomSheetDialog(mContext);
        signUpDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        signUpDialog.setContentView(view);
        signUpBinding.termsAndConditionsText.setPaintFlags(signUpBinding.termsAndConditionsText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        signUpBinding.termsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openTermsAndConditionActivity();
            }
        });
        signUpBinding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser(signUpBinding);
            }
        });
        signUpDialog.show();
    }

    private void registerUser(BottomSheetSignUpBinding signUpBinding) {
        String email, password, firstName, lastName, phone;
        email = signUpBinding.email.getText().toString().trim();
        password = signUpBinding.password.getText().toString().trim();
        firstName = signUpBinding.firstName.getText().toString().trim();
        lastName = signUpBinding.surname.getText().toString().trim();
        phone = signUpBinding.phone.getText().toString().trim();

        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            signUpBinding.email.setError(getString(R.string.email_validation));
            return;
        } else {
            signUpBinding.email.setError(null);
        }

        if (!Utils.isPasswordValidMethod(password)) {
            signUpBinding.password.setError(getString(R.string.password_validation));
            return;
        } else {
            signUpBinding.password.setError(null);
        }

        if (TextUtils.isEmpty(firstName)) {
            signUpBinding.firstName.setError(getString(R.string.first_name_validation));
            return;
        } else {
            signUpBinding.firstName.setError(null);
        }

        if (TextUtils.isEmpty(lastName)) {
            signUpBinding.surname.setError(getString(R.string.last_name_validation));
            return;
        } else {
            signUpBinding.surname.setError(null);
        }

        if (TextUtils.isEmpty(phone)) {
            signUpBinding.phone.setError(getString(R.string.phone_validation));
            return;
        } else {
            signUpBinding.phone.setError(null);
        }

        RegisterUser registerUser = new RegisterUser();
        registerUser.setEmail(email);
        registerUser.setPassword(password);
        registerUser.setFirstName(firstName);
        registerUser.setLastName(lastName);
        registerUser.setPhoneNumber(phone);
        if(Utils.isNetworkAvailable(mContext)) {
            registerUserRequest(registerUser);
        } else {
            showNoInternetErrorMessage();
        }
    }

    private void registerUserRequest(RegisterUser registerUser) {
        progressDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.register(registerUser).enqueue(new Callback<TokenResponse>() {

            @Override
            public void onResponse(@NonNull Call<TokenResponse> call, @NonNull Response<TokenResponse> response) {
                progressDialog.hideProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    final TokenResponse tokenResponse = response.body();
                    if (tokenResponse.getStatusCode() == 403) {
                        DialogUtility.showDialog(mContext, tokenResponse.getMessage(), Constants.ERROR);
                    } else {
                        final String accessToken = tokenResponse.getAccessToken();
                        if (!TextUtils.isEmpty(accessToken)) {
                            DialogUtility.showDialog(mContext, getString(R.string.register_successful), Constants.SUCCESS, new DialogActionListener() {
                                @Override
                                public void onClick() {
                                    mSharedPreferenceUtility.setAccessToken(accessToken);
                                    mSharedPreferenceUtility.setMyUserId(tokenResponse.getUserId());
                                    getProfileInfo(true);
                                    progressDialog.showProgress();
                                }
                            });
                        } else {
                            DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                        }

                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call,@NonNull Throwable t) {
                progressDialog.hideProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void openLoginDialog() {
        final BottomSheetLoginBinding loginBinding = BottomSheetLoginBinding.inflate(LayoutInflater.from(mContext));
        View view = loginBinding.getRoot();
        loginDialog = new BottomSheetDialog(mContext);
        loginDialog.setContentView(view);
        loginDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        loginBinding.forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openForgotPasswordDialog();
                loginDialog.dismiss();
            }
        });
        loginBinding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser(loginBinding);
            }
        });

        loginBinding.updatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPasswordResetDialog();
                loginDialog.dismiss();
            }
        });
        loginDialog.show();
    }

    private void openPasswordResetDialog() {
        final BottomSheetPasswordResetBinding passwordResetBinding = BottomSheetPasswordResetBinding.inflate(LayoutInflater.from(mContext));
        View view = passwordResetBinding.getRoot();
        passwordResetDialog = new BottomSheetDialog(mContext);
        passwordResetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        passwordResetDialog.setContentView(view);
        mBinding.startLayout.setVisibility(View.GONE);
        passwordResetBinding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordReset(passwordResetBinding);

            }
        });
        passwordResetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                passwordResetDialog.dismiss();
                mBinding.startLayout.setVisibility(View.VISIBLE);
            }
        });
        passwordResetDialog.show();
    }

    private void passwordReset(BottomSheetPasswordResetBinding passwordResetBinding) {
        String code, password;
        code = passwordResetBinding.code.getText().toString().trim();
        password = passwordResetBinding.password.getText().toString().trim();

        if (TextUtils.isEmpty(code)) {
            passwordResetBinding.code.setError(getString(R.string.enter_valid_code));
            return;
        } else {
            passwordResetBinding.code.setError(null);
        }

        if (!Utils.isPasswordValidMethod(password)) {
            passwordResetBinding.password.setError(getString(R.string.password_validation));
            return;
        } else {
            passwordResetBinding.password.setError(null);
        }

        RegisterUser registerUser = new RegisterUser();
        registerUser.setCode(code);
        registerUser.setPassword(password);
        if(Utils.isNetworkAvailable(mContext)) {
            sendPasswordResetRequest(registerUser);
        } else {
            showNoInternetErrorMessage();
        }
    }

    private void sendPasswordResetRequest(RegisterUser registerUser) {
        progressDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.resetPassword(registerUser).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call,@NonNull Response<TokenResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    TokenResponse tokenResponse = response.body();
                    if (tokenResponse.getStatusCode() == 403) {
                        DialogUtility.showDialog(mContext, tokenResponse.getMessage(), Constants.ERROR);
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.password_reset), Constants.SUCCESS, new DialogActionListener() {
                            @Override
                            public void onClick() {
                                passwordResetDialog.dismiss();
                            }
                        });

                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
                progressDialog.hideProgress();
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call,@NonNull Throwable t) {
                progressDialog.hideProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void loginUser(BottomSheetLoginBinding loginBinding) {
        String email, password;
        email = loginBinding.email.getText().toString().trim();
        password = loginBinding.password.getText().toString().trim();

        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginBinding.email.setError(getString(R.string.email_validation));
            return;
        } else {
            loginBinding.email.setError(null);
        }
        if (!Utils.isPasswordValidMethod(password)) {
            loginBinding.password.setError(getString(R.string.password_validation));
            return;
        } else {
            loginBinding.password.setError(null);
        }
        RegisterUser registerUser = new RegisterUser();
        registerUser.setEmail(email);
        registerUser.setPassword(password);
        if(Utils.isNetworkAvailable(mContext)) {
            login(registerUser);
        } else {
            showNoInternetErrorMessage();
        }
    }

    private void login(RegisterUser registerUser) {
        progressDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.login(registerUser).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call,@NonNull Response<TokenResponse> response) {
                progressDialog.hideProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    TokenResponse tokenResponse = response.body();
                    if (tokenResponse.getStatusCode() == 403) {
                        progressDialog.hideProgress();
                        DialogUtility.showDialog(mContext, tokenResponse.getMessage(), Constants.ERROR);
                    } else {
                        String accessToken = tokenResponse.getAccessToken();
                        if (!TextUtils.isEmpty(accessToken)) {
                            mSharedPreferenceUtility.setAccessToken(accessToken);
                            mSharedPreferenceUtility.setMyUserId(tokenResponse.getUserId());
                            loginDialog.dismiss();
                            sendDeviceIdToServer();
                        } else {
                            DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                        }
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }

            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call,@NonNull Throwable t) {
                progressDialog.hideProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }


    private void openForgotPasswordDialog() {
        final BottomSheetForgotPasswordBinding forgotPasswordBinding = BottomSheetForgotPasswordBinding.inflate(LayoutInflater.from(mContext));
        View view = forgotPasswordBinding.getRoot();
        forgotPasswordDialog = new BottomSheetDialog(mContext);
        forgotPasswordDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        forgotPasswordDialog.setContentView(view);
        mBinding.startLayout.setVisibility(View.GONE);
        forgotPasswordBinding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPassword(forgotPasswordBinding);

            }
        });
        forgotPasswordDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                forgotPasswordDialog.dismiss();
                mBinding.startLayout.setVisibility(View.VISIBLE);
            }
        });
        forgotPasswordDialog.show();
    }

    private void forgotPassword(BottomSheetForgotPasswordBinding forgotPasswordBinding) {
        String email = forgotPasswordBinding.email.getText().toString().trim();
        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            forgotPasswordBinding.email.setError(getString(R.string.email_validation));
            return;
        } else {
            forgotPasswordBinding.email.setError(null);
        }
        RegisterUser registerUser = new RegisterUser();
        registerUser.setEmail(email);
        if(Utils.isNetworkAvailable(mContext)) {
            sendForgotPasswordRequest(registerUser);
        } else {
            showNoInternetErrorMessage();
        }

    }

    private void sendForgotPasswordRequest(RegisterUser registerUser) {
        progressDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.forgotPassword(registerUser).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call,@NonNull Response<TokenResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    TokenResponse tokenResponse = response.body();
                    if (tokenResponse.getStatusCode() == 403) {
                        DialogUtility.showDialog(mContext, tokenResponse.getMessage(), Constants.ERROR);
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.sent_code), Constants.SUCCESS, new DialogActionListener() {
                            @Override
                            public void onClick() {
                                forgotPasswordDialog.dismiss();
                                openPasswordResetDialog();
                            }
                        });
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
                progressDialog.hideProgress();
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call,@NonNull Throwable t) {
                progressDialog.hideProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initializeFacebookLogin() {
        LoginManager.getInstance().logOut();
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        final AccessToken accessToken = AccessToken.getCurrentAccessToken();
                        Log.d("SUCCESS", "FACEBOOK LOGIN WORKS " + accessToken.getToken());
                        GraphRequest.newGraphPathRequest(loginResult.getAccessToken(), "/me?fields=id,first_name,email,last_name", new GraphRequest.Callback() {
                            @Override
                            public void onCompleted(GraphResponse response) {
                                if (response.getError() != null) {
                                    showFacebookError();
                                } else {
                                    JSONObject me = response.getJSONObject();
                                    String email = me.optString("email");
                                    String firstName = me.optString("first_name");
                                    String lastName = me.optString("last_name");
                                    email = email.replace('\u0040', '@');
                                    if (TextUtils.isEmpty(email)) {
                                        showFacebookError();
                                    } else {
                                        RegisterUser registerUser = new RegisterUser();
                                        registerUser.setEmail(email);
                                        registerUser.setFirstName(firstName);
                                        registerUser.setLastName(lastName);
                                        registerUser.setAccessToken(accessToken.getToken());
                                        signIn(registerUser);
                                    }
                                }
                            }
                        }).executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException e) {
                        showFacebookError();
                    }
                });
    }

    private void signIn(RegisterUser registerUser) {
        progressDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.facebookLogin(registerUser).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call,@NonNull Response<TokenResponse> response) {
                progressDialog.hideProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    TokenResponse tokenResponse = response.body();
                    if(tokenResponse != null) {
                        if (tokenResponse.getStatusCode() == 403) {
                            DialogUtility.showDialog(mContext, tokenResponse.getMessage(), Constants.ERROR);
                        } else {
                            String accessToken = tokenResponse.getAccessToken();
                            if (!TextUtils.isEmpty(accessToken)) {
                                mSharedPreferenceUtility.setAccessToken(accessToken);
                                mSharedPreferenceUtility.setMyUserId(tokenResponse.getUserId());
                                sendDeviceIdToServer();
                            } else {
                                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                            }
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call,@NonNull Throwable t) {
                progressDialog.hideProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void showFacebookError() {
        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
    }

}
