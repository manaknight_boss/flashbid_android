package com.flashbid.flashcoinapp.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.ActivityMainBinding;
import com.flashbid.flashcoinapp.databinding.BottomSheetScanSelectionBinding;
import com.flashbid.flashcoinapp.fragments.AboutFragment;
import com.flashbid.flashcoinapp.fragments.BaseFragment;
import com.flashbid.flashcoinapp.fragments.DetailFragment;
import com.flashbid.flashcoinapp.fragments.EditProfileFragment;
import com.flashbid.flashcoinapp.fragments.FidelityFragment;
import com.flashbid.flashcoinapp.fragments.FlashPayOtpFragment;
import com.flashbid.flashcoinapp.fragments.HistoryFragment;
import com.flashbid.flashcoinapp.fragments.HomeFragment;
import com.flashbid.flashcoinapp.fragments.QRCodeFragment;
import com.flashbid.flashcoinapp.fragments.StickerOtpFragment;
import com.flashbid.flashcoinapp.fragments.UserOtpFragment;
import com.flashbid.flashcoinapp.interfaces.DialogActionListener;
import com.flashbid.flashcoinapp.interfaces.FragmentNavigation;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.interfaces.PermissionRequestCallBacks;
import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.ApiResponse;
import com.flashbid.flashcoinapp.models.Base32;
import com.flashbid.flashcoinapp.models.Contests;
import com.flashbid.flashcoinapp.models.Results;
import com.flashbid.flashcoinapp.models.ScanCode;
import com.flashbid.flashcoinapp.models.Sticker;
import com.flashbid.flashcoinapp.models.TokenResponse;
import com.flashbid.flashcoinapp.models.UserDetail;
import com.flashbid.flashcoinapp.models.firebaserereferences.User;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;
import com.squareup.picasso.Picasso;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, FragNavController.TransactionListener, FragmentNavigation, FragNavController.RootFragmentListener, HomeListener {
    private static final int GALLERY_REQUEST = 21;
    private ActivityMainBinding mBinding;
    private UserDetail mUserDetail;
    private FragNavController mNavController;
    private AmazonS3Client s3Client;
    private BarcodeDetector detector;
    private BottomSheetDialog scanSelectionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        init(savedInstanceState);
        mBinding.navigationView.setNavigationItemSelectedListener(this);
        s3Client = new AmazonS3Client(new BasicAWSCredentials(Constants.KEY, Constants.SECRET));
        setupFirebaseForProfile();
        setupFirebaseForMaintenance();
        if(Utils.isNetworkAvailable(mContext)) {
            if(TextUtils.isEmpty(mSharedPreferenceUtility.getQrCode())) {
                getProfileInfo(false);
            }
        } else {
            showNoInternetErrorMessage();
        }

    }

    private void setupFirebaseForMaintenance() {
        final DatabaseReference maintenanceReference = mDatabase.child(Constants.FirebaseReferences.MAINTENANCE);
        maintenanceReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Object object = dataSnapshot.getValue();
                if(object instanceof Long) {
                    long maintenanceStatus = (Long)object;
                    checkMaintenanceStatus(maintenanceStatus);
                }

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    Object object = dataSnapshot.getValue();
                    if(object instanceof List) {
                        List<Object> maintenanceList = (List<Object>) object;
                        if (maintenanceList.size() > 0) {
                            long maintenanceStatus = (Long) maintenanceList.get(1);
                            checkMaintenanceStatus(maintenanceStatus);
                        }
                    }
                }catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        maintenanceReference.addListenerForSingleValueEvent(eventListener);
    }

    private void checkMaintenanceStatus(long maintenanceStatus) {
        WeakReference<MainActivity> mainActivityWeakRef = new WeakReference<>(MainActivity.this);
        if (maintenanceStatus == Constants.UNDER_MAINTENANCE) {
            if (mainActivityWeakRef.get() != null && !mainActivityWeakRef.get().isFinishing()) {
                DialogUtility.showDialog(mContext, getString(R.string.under_maintenance), Constants.ERROR, new DialogActionListener() {
                    @Override
                    public void onClick() {
                        finish();
                    }
                });
            }
        }
    }


    private void setupFirebaseForProfile() {
        final DatabaseReference user = mDatabase.child(Constants.FirebaseReferences.USER).child("" + mSharedPreferenceUtility.getMyUserId());
        user.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String key = dataSnapshot.getKey();
                if(!TextUtils.isEmpty(key)) {
                    extractProfileDataFromSnapshot(dataSnapshot.getValue(),key);
                    setData();
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> userMap = (Map<String, Object>) dataSnapshot.getValue();
                mUserDetail = new UserDetail();
                mUserDetail.setFirstName((String) userMap.get(User.FIRST_NAME));
                mUserDetail.setLastName((String) userMap.get(User.LAST_NAME));
                mUserDetail.setEmail((String) userMap.get(User.EMAIL));
                mUserDetail.setPhoneNumber(""+userMap.get(User.PHONE_NUMBER));
                mUserDetail.setProfileImage((String) userMap.get(User.PROFILE_IMAGE));
                mUserDetail.setAddress((String) userMap.get(User.ADDRESS));
                Object balance = userMap.get(User.BALANCE);
                if(balance instanceof Double) {
                    mUserDetail.setBalance((double)balance);
                } else {
                    mUserDetail.setBalance((long)balance);
                }
                mSharedPreferenceUtility.setUserWalletBalance(mUserDetail.getBalance());
                Object moneyOwed = userMap.get(User.MONEY_OWED);
                if(moneyOwed instanceof Double) {
                    mUserDetail.setMoneyOwed((double) moneyOwed);
                } else {
                    mUserDetail.setMoneyOwed((long)moneyOwed);
                }
                mUserDetail.setCity((String) userMap.get(User.CITY));
                mUserDetail.setCountry((String) userMap.get(User.COUNTRY));
                mUserDetail.setPostalCode((String) userMap.get(User.POSTAL_CODE));
                mUserDetail.setState((String) userMap.get(User.STATE));
                mUserDetail.setStatus((String) userMap.get(User.STATUS));
                UserDetail userDetail = new Gson().fromJson((String) userMap.get(User.DATA), UserDetail.class);
                mUserDetail.setGender(userDetail.getGender());
                mUserDetail.setAgeGroup(userDetail.getAgeGroup());
                setData();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        user.addListenerForSingleValueEvent(eventListener);
    }

    private void extractProfileDataFromSnapshot(Object value, String key) {
        if(key.equals(User.FIRST_NAME)) {
            mUserDetail.setFirstName((String)value);
        } else if(key.equals(User.LAST_NAME)) {
            mUserDetail.setLastName((String)value);
        } else if(key.equals(User.EMAIL)) {
            mUserDetail.setEmail((String)value);
        } else if(key.equals(User.PHONE_NUMBER)) {
            mUserDetail.setPhoneNumber((String)value);
        } else if(key.equals(User.ADDRESS)) {
            mUserDetail.setAddress((String)value);
        } else if(key.equals(User.STATE)) {
            mUserDetail.setState((String)value);
        } else if(key.equals(User.CITY)) {
            mUserDetail.setCity((String)value);
        } else if(key.equals(User.POSTAL_CODE)) {
            mUserDetail.setPostalCode((String)value);
        } else if(key.equals(User.DATA)) {
            UserDetail userDetail = new Gson().fromJson((String)value, UserDetail.class);
            mUserDetail.setGender(userDetail.getGender());
            mUserDetail.setAgeGroup(userDetail.getAgeGroup());
        } else if(key.equals(User.PROFILE_IMAGE)) {
            mUserDetail.setProfileImage((String)value);
        } else if(key.equals(User.BALANCE)) {
            if(value instanceof Double) {
                mUserDetail.setBalance((double)value);
            } else {
                mUserDetail.setBalance((long)value);
            }
            mSharedPreferenceUtility.setUserWalletBalance(mUserDetail.getBalance());
        } else if(key.equals(User.MONEY_OWED)) {
            if(value instanceof Double) {
                mUserDetail.setMoneyOwed((double) value);
            } else {
                mUserDetail.setMoneyOwed((long)value);
            }
        }
    }

    private void setData() {
        View header = mBinding.navigationView.getHeaderView(0);
        TextView price = header.findViewById(R.id.price);
        TextView name = header.findViewById(R.id.name);
        ImageView profileImage = header.findViewById(R.id.profile_image);
        if (!TextUtils.isEmpty(mUserDetail.getProfileImage())) {
            Picasso.get().load(mUserDetail.getProfileImage()).placeholder(R.mipmap.profile).into(profileImage);
        } else {
            profileImage.setImageResource(R.mipmap.profile);
        }
        name.setText(mUserDetail.getFirstName() + " " + mUserDetail.getLastName());
        price.setText(Utils.roundOffTo2DecPlaces(mUserDetail.getBalance()));
    }


    private void logoutUser() {
        DialogUtility.showProgressDialog(mContext, getString(R.string.please_wait));
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setAction(Constants.ActionName.LOGOUT);
        apiRequest.setParameter(Constants.NO_PARAMETERS);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.logout(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ApiResponse apiResponse = response.body();
                    if (apiResponse != null && apiResponse.getStatusCode() == 403) {
                        DialogUtility.showDialog(mContext, apiResponse.getMessage(), Constants.ERROR);
                    } else {
                        mSharedPreferenceUtility.clear();
                        openStartActivity();
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.drawer_edit_profile) {
            openEditProfileFragment();
        } else if (id == R.id.drawer_my_wallet) {

        } else if (id == R.id.drawer_scan) {
            openScanSelectionDialog();
        } else if (id == R.id.drawer_record) {
            openHistoryFragment();
        } else if (id == R.id.drawer_code) {
            pushFragment(QRCodeFragment.newInstance());
        } else if (id == R.id.drawer_info) {
            pushFragment(AboutFragment.newInstance());
        } else if (id == R.id.drawer_close) {
            if(Utils.isNetworkAvailable(mContext)) {
                logoutUser();
            } else {
                showNoInternetErrorMessage();
            }
        }

        mBinding.drawer.closeDrawers();
        return true;
    }

    private void openHistoryFragment() {
        HistoryFragment historyFragment = HistoryFragment.newInstance();
        pushFragment(historyFragment);
    }

    private void openEditProfileFragment() {
        EditProfileFragment editProfileFragment = EditProfileFragment.newInstance();
        pushFragment(editProfileFragment);
    }

    @Override
    public void pushFragment(Fragment fragment, FragNavTransactionOptions transactionOptions) {

    }


    void init(Bundle savedInstanceState) {
        HomeFragment homeFragment = HomeFragment.newInstance();
        FragNavController.Builder builder = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.container).transactionListener(this).rootFragment(homeFragment);
        mNavController = builder.build();
    }

    @Override
    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }

    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        if (getSupportActionBar() != null && mNavController != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        }
    }

    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType
            transactionType) {
        if (getSupportActionBar() != null && mNavController != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        }
    }

    public UserDetail getMyProfile() {
        return mUserDetail;
    }

    @Override
    public void openGalleryFor(final int requestCode, final int imageCount, final boolean captureRequired) {
        checkPermissionForMarshMellow(new PermissionRequestCallBacks() {
                                          @Override
                                          public void onPermissionsGranted() {
                                              Matisse.from(MainActivity.this)
                                                      .choose(MimeType.ofImage())
                                                      .capture(captureRequired)
                                                      .captureStrategy(new CaptureStrategy(true, getPackageName() + ".fileProvider"))
                                                      .maxSelectable(imageCount)
                                                      .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                                                      .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                                                      .thumbnailScale(0.85f)
                                                      .imageEngine(new GlideEngine())
                                                      .forResult(requestCode);
                                          }

                                          @Override
                                          public void onPermissionsDenied() {
                                          }
                                      }, getString(R.string.storage_permission_required), Manifest.permission.WRITE_EXTERNAL_STORAGE
                , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }

    @Override
    public void onDrawerMenuClick() {
        mBinding.drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public Fragment getRootFragment(int i) {
        return HomeFragment.newInstance();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null && data != null) {
            String result = data.getStringExtra("SCAN_RESULT");
            decodeBase32AndRequestData(result);
        } else if (requestCode == Constants.EDIT_PROFILE_IMAGE && resultCode == RESULT_OK && data != null) {
            List<String> images = Matisse.obtainPathResult(data);
            try {
                if (images != null && images.size() > 0) {
                    Intent intent = new Intent(mContext, Cropper.class);
                    intent.putExtra(Constants.IMAGE, images.get(0));
                    startActivityForResult(intent, Constants.REQUEST_CROPPER);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.REQUEST_CROPPER && resultCode == RESULT_OK && data != null) {
            String filePath = data.getStringExtra(Constants.CROPPED_IMAGE_EXTRA);
            try {
                File file = new File(filePath);
                if (file.exists()) {
                    Bitmap croppedImageBitmap = getBitmapFromImagePath(file.getAbsolutePath());
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(croppedImageBitmap, Constants.MAX_PROFILE_PHOTO_WIDTH, Constants.MAX_PROFILE_PHOTO_HEIGHT, true);
                    String imagePath = writeBitmapToLocalPath(bMapScaled, Constants.COMPRESS_80);
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                    if (fragment instanceof EditProfileFragment) {
                        EditProfileFragment editProfileFragment = (EditProfileFragment) fragment;
                        editProfileFragment.setPhoto(imagePath);
                    }
                    if(Utils.isNetworkAvailable(mContext)) {
                        sendImageToServer(imagePath);
                    } else {
                        showNoInternetErrorMessage();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            List<String> imagePath = Matisse.obtainPathResult(data);
            try {
                Bitmap bitmap = getBitmapFromImagePath(imagePath.get(0));
                if (detector.isOperational() && bitmap != null) {
                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                    SparseArray<Barcode> barcodes = detector.detect(frame);
                    for (int index = 0; index < barcodes.size(); index++) {
                        Barcode code = barcodes.valueAt(index);
                        decodeBase32AndRequestData(code.displayValue);
                    }
                    if (barcodes.size() == 0) {
                        DialogUtility.showDialog(mContext,getString(R.string.code_not_found),Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext,getString(R.string.something_went_wrong),Constants.ERROR);
                }
            } catch (Exception e) {
                DialogUtility.showDialog(mContext,getString(R.string.something_went_wrong),Constants.ERROR);
            }
        }
    }

    private void decodeBase32AndRequestData(String qrCode) {
        showScreenProgress();
        scanSelectionDialog.dismiss();
        try {

            byte bytes[];
            try {
                bytes = Base32.decode(qrCode);
            }catch (Exception ex) {
                bytes = com.amazonaws.util.Base32.decode(qrCode);
            }
            String data[] = new String(bytes).split(":");
            String code = data[0];
            String type = data[1];
            Log.d("QR_CODE", qrCode);
            Log.d("QR_CODE", "Code : "+code);
            Log.d("QR_CODE", "type : "+type);
            type = type.toLowerCase();
            switch (type) {
                case Constants.ScanType.USER:
                    getUserFromQRCode(code);
                    break;
                case Constants.ScanType.STICKER:
                    getStickerFromQRCode(code);
                    break;
                case Constants.ScanType.PAY:
                    getFlashPayFromQRCode(code);
                    break;
                case Constants.ScanType.FOTP:
                case Constants.ScanType.OTP:
                case Constants.ScanType.LOTTERY:
                case Constants.ScanType.AUCTION:
                    getOTPFromQRCode(code,type);
                    break;
            }
        } catch (Exception ex) {
            hideScreenProgress();
            DialogUtility.showDialog(this, getString(R.string.invalid_code),Constants.ERROR);
        }
    }

    private void getUserFromQRCode(String code) {
        ApiRequest apiRequest = new ApiRequest();
        ScanCode scanCode = new ScanCode(code,Constants.ScanType.USER);
        apiRequest.setParameter(new Gson().toJson(scanCode));
        apiRequest.setAction(Constants.ActionName.CHECK_CODE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getProfileInfo(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(@NonNull Call<UserDetail> call, @NonNull Response<UserDetail> response) {
                hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    UserDetail userDetailResponse = response.body();
                    if(userDetailResponse != null) {
                        if (userDetailResponse.getStatusCode() == 403 || userDetailResponse.getStatusCode() == 404) {
                            DialogUtility.showDialog(mContext, userDetailResponse.getMessage(), Constants.ERROR);
                        } else {
                            pushFragment(UserOtpFragment.newInstance(userDetailResponse));
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserDetail> call, @NonNull Throwable t) {
                hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void getFlashPayFromQRCode(String code) {
        ApiRequest apiRequest = new ApiRequest();
        ScanCode scanCode = new ScanCode(code,Constants.ScanType.PAY);
        apiRequest.setParameter(new Gson().toJson(scanCode));
        apiRequest.setAction(Constants.ActionName.CHECK_CODE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getItemDetail(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<Contests>() {
            @Override
            public void onResponse(@NonNull Call<Contests> call, @NonNull Response<Contests> response) {
                hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    Contests contestResponse = response.body();
                    if(contestResponse != null) {
                        if (contestResponse.getStatusCode() == 403 || contestResponse.getStatusCode() == 404) {
                            DialogUtility.showDialog(mContext, contestResponse.getMessage(), Constants.ERROR);
                        } else {
                            openFlashPayScreen(contestResponse.getId());
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Contests> call, @NonNull Throwable t) {
                hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void getOTPFromQRCode(String code, String type) {
        ApiRequest apiRequest = new ApiRequest();
        ScanCode scanCode = new ScanCode(code,type);
        apiRequest.setParameter(new Gson().toJson(scanCode));
        apiRequest.setAction(Constants.ActionName.CHECK_CODE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getItemDetail(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<Contests>() {
            @Override
            public void onResponse(@NonNull Call<Contests> call, @NonNull Response<Contests> response) {
                hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    Contests contestResponse = response.body();
                    if(contestResponse != null) {
                        if (contestResponse.getStatusCode() == 403 || contestResponse.getStatusCode() == 404) {
                            DialogUtility.showDialog(mContext, contestResponse.getMessage(), Constants.ERROR);
                        } else {
                            openItemDetail(contestResponse);
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Contests> call, @NonNull Throwable t) {
                hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void getStickerFromQRCode(final String code) {
        ApiRequest apiRequest = new ApiRequest();
        ScanCode scanCode = new ScanCode(code,Constants.ScanType.STICKER);
        apiRequest.setParameter(new Gson().toJson(scanCode));
        apiRequest.setAction(Constants.ActionName.CHECK_CODE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getStickerDetail(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<Sticker>() {
            @Override
            public void onResponse(@NonNull Call<Sticker> call, @NonNull Response<Sticker> response) {
                hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    Sticker stickerResponse = response.body();
                    if(stickerResponse != null) {
                        if (stickerResponse.getStatusCode() == 403 || stickerResponse.getStatusCode() == 404) {
                            DialogUtility.showDialog(mContext, stickerResponse.getMessage(), Constants.ERROR);
                        } else {
                            openStickerDetail(stickerResponse);
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Sticker> call, @NonNull Throwable t) {
                hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void sendImageToServer(final String imagePath) {
        TransferUtility transferUtility =
                TransferUtility.builder().defaultBucket(Constants.S3_IMAGE_BUCKET)
                        .context(this)
                        .s3Client(s3Client)
                        .build();

        final File file = new File(imagePath);
        final String fileName = Constants.IMAGE_INITIAL + System.currentTimeMillis() + "_" + mSharedPreferenceUtility.getMyUserId() + ".jpg";
        TransferObserver uploadObserver = transferUtility.upload(fileName, file);
        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    String newUrl = Constants.S3_IMAGE_BASE_URL + Constants.S3_IMAGE_BUCKET + "/" + fileName;
                    saveImageUrlToServer(newUrl);
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                    if (fragment instanceof EditProfileFragment) {
                        EditProfileFragment editProfileFragment = (EditProfileFragment) fragment;
                        editProfileFragment.setPhotoURL(newUrl);
                    }
                    file.deleteOnExit();
                } else if (TransferState.FAILED == state) {
                    file.deleteOnExit();
                    progressDialog.hideProgress();
                } else if (TransferState.IN_PROGRESS == state) {
                    progressDialog.showProgress();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {
                ex.printStackTrace();
            }

        });

    }

    private void saveImageUrlToServer(String imageUrl) {
        UserDetail userDetail = new UserDetail();
        userDetail.setProfileImage(imageUrl);
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setParameter(new Gson().toJson(userDetail));
        apiRequest.setAction(Constants.ActionName.PROFILE_IMAGE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.updateProfileDetail(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call, @NonNull Response<TokenResponse> response) {
                hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    TokenResponse tokenResponse = response.body();
                    if (tokenResponse != null && tokenResponse.getStatusCode() == 403) {
                        DialogUtility.showDialog(mContext, tokenResponse.getMessage(), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call, @NonNull Throwable t) {
                hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    public void showScreenProgress() {
        progressDialog.showProgress();
    }

    public void hideScreenProgress() {
        progressDialog.hideProgress();
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        } else {
            hideKeyboard(this);
            popFragment();
        }
    }

    @Override
    public void popFragment() {
        if (!mNavController.isRootFragment()) {
            mNavController.popFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public DatabaseReference getFirebaseDatabase() {
        return mDatabase;
    }

    @Override
    public void openServiceFragment(int serviceType) {
        if(serviceType == Constants.SERVICE_TYPE_SURVEY) {
            pushFragment(new BaseFragment());
        } else if(serviceType == Constants.SERVICE_TYPE_REFILL) {
            pushFragment(new BaseFragment());
        } else {
            pushFragment(FidelityFragment.newInstance());
        }
    }

    public void openScanSelectionDialog() {
        BottomSheetScanSelectionBinding scanSelectionBinding = BottomSheetScanSelectionBinding.inflate(LayoutInflater.from(mContext));
        View view = scanSelectionBinding.getRoot();
        scanSelectionDialog = new BottomSheetDialog(mContext);
        scanSelectionDialog.setContentView(view);
        scanSelectionDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        scanSelectionBinding.scanFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initQRCodeReader();
                if(detector.isOperational()) {
                    openGalleryFor(GALLERY_REQUEST, 1, false);
                } else {
                    DialogUtility.showDialog(mContext,getString(R.string.scan_not_working),Constants.ERROR);
                }
            }
        });
        scanSelectionBinding.scanWithCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                integrator.initiateScan();
            }
        });
        scanSelectionDialog.show();
    }

    @Override
    public void openItemDetail(Contests contest) {
        DetailFragment detailFragment = DetailFragment.newInstance();
        if(contest.getType().equalsIgnoreCase(Results.OTP)) {
            detailFragment.setItemId(contest.getId(),true);
        } else {
            detailFragment.setItemId(contest.getId());
        }
        pushFragment(detailFragment);
    }

    public void openStickerDetail(Sticker sticker) {
        StickerOtpFragment stickerOtpFragment = new StickerOtpFragment();
        stickerOtpFragment.setItem(sticker);
        pushFragment(stickerOtpFragment);
    }

    public void openFlashPayScreen(long id) {
        FlashPayOtpFragment flashPayOtpFragment = new FlashPayOtpFragment();
        flashPayOtpFragment.setItemId(id);
        pushFragment(flashPayOtpFragment);
    }

    private void initQRCodeReader() {
        detector = new BarcodeDetector.Builder(getApplicationContext())
                        .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                        .build();
    }
}
