package com.flashbid.flashcoinapp.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.utils.Constants;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Cropper extends BaseActivity {

    private CropImageView cropImageView;
    private Bitmap croppedImage;
    ImageView mCrossImageView, rotateImage;
    TextView thumbnailText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cropper);
        thumbnailText = (TextView) findViewById(R.id.thumbnail_text);
        cropImageView = (CropImageView) findViewById(R.id.ImageView);
        mCrossImageView = (ImageView) findViewById(R.id.close_activity_imageview);
        rotateImage = (ImageView) findViewById(R.id.rotate_image);

        mCrossImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView cover = (ImageView) findViewById(R.id.done);
        String path = getIntent().getStringExtra(getString(R.string.image));
        Bitmap bitmap = null;
        File f = new File(path);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        cropImageView.setImageBitmap(bitmap);

        rotateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cropImageView.rotateImage(90);
            }
        });

        cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                croppedImage = cropImageView.getCroppedImage();
                sendCroppedResult(croppedImage);
            }
        });


    }

    void sendCroppedResult(Bitmap bitmap) {
        Intent intent = new Intent();
        String filePath = writeBitmapToLocalPath(bitmap, Constants.NO_COMPRESSION);
        intent.putExtra(Constants.CROPPED_IMAGE_EXTRA, filePath);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
