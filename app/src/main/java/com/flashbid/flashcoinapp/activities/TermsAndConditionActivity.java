package com.flashbid.flashcoinapp.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.ActivityTermsAndConditionBinding;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TermsAndConditionActivity extends BaseActivity {
    private ActivityTermsAndConditionBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_terms_and_condition);
        mBinding.okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        DatabaseReference termsReference = mDatabase.child(Constants.FirebaseReferences.TERMS_AND_CONDITIONS);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String description = ds.child("description").getValue(String.class);
                    mBinding.progressBar.setVisibility(View.GONE);
                    mBinding.termsLayout.setVisibility(View.VISIBLE);
                    mBinding.message.setText(description);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mBinding.message.setText(getString(R.string.terms_and_conditions));
                mBinding.termsLayout.setVisibility(View.VISIBLE);
                mBinding.progressBar.setVisibility(View.GONE);
            }
        };
        if(Utils.isNetworkAvailable(mContext)) {
            termsReference.addListenerForSingleValueEvent(eventListener);
        } else {
            showNoInternetErrorMessage();
        }
    }
}
