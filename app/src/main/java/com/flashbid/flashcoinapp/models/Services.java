package com.flashbid.flashcoinapp.models;

public class Services {
    private String serviceName;
    private int image;
    private String message;

    public Services(String serviceName, int image, String message) {
        this.serviceName = serviceName;
        this.image = image;
        this.message = message;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
