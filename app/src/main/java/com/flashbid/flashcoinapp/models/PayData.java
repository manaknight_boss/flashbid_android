package com.flashbid.flashcoinapp.models;

public class PayData {
    private long threshold;
    private long redeem;

    public long getThreshold() {
        return threshold;
    }

    public void setThreshold(long threshold) {
        this.threshold = threshold;
    }

    public long getRedeem() {
        return redeem;
    }

    public void setRedeem(long redeem) {
        this.redeem = redeem;
    }
}
