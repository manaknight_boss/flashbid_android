package com.flashbid.flashcoinapp.models.firebaserereferences;

public interface Result extends UserBase {
    String WINNERS = "winners";
    String UPDATED_AT = "updated_at";
    String MESSAGE = "message";
    String TYPE = "type";
    String STATUS = "status";
    String IMAGE = "image";
    String FINAL_BID = "final_bid";
    String TITLE = "title";
    String NAME = "name";
}
