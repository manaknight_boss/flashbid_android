package com.flashbid.flashcoinapp.models.firebaserereferences;

public interface Contest {

    String ROOM_ID = "room_id";
    String DATA = "data";
    String FINAL_BID = "final_bid";
    String SECONDARY_IMAGE = "secondary_image";
    String CREATED_AT = "created_at";
    String DESCRIPTION = "description";
    String WINNER_ID = "winner_id";
    String INITIAL_BID = "initial_bid";
    String TITLE = "title";
    String TYPE = "type";
    String BID_AMOUNT = "bid_amount";
    String UPDATED_AT = "updated_at";
    String QR_CODE = "qr_code";
    String ID = "id";
    String STOCK = "stock";
    String DELIVERY = "delivery";
    String IMAGE = "image";
    String MARKET_VALUE = "market_value";
    String TICKET_SOLD = "ticket_sold";
    String SIZE = "size";
    String BUY_AT_AMOUNT = "buy_at_amount";
    String TICKET_AMOUNT = "ticket_amount";
    String TICKET_TOTAL = "ticket_total";
    String ITEM_WORTH = "item_worth";
    String STATUS = "status";
}
