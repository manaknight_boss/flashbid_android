package com.flashbid.flashcoinapp.models.firebaserereferences;

public interface UserBase {
    String ID = "id";
    String FIRST_NAME = "first_name";
    String LAST_NAME = "last_name";
    String PROFILE_IMAGE = "profile_image";
}
