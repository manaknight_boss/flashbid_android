package com.flashbid.flashcoinapp.models;

import com.google.gson.annotations.SerializedName;

public class Sticker extends ApiResponse {
    private long id;
    private double amount;
    @SerializedName("vendor_id")
    private long vendorId;
    @SerializedName("shop_title")
    private String shopTitle;
    @SerializedName("shop_country")
    private String shopCountry;
    @SerializedName("shop_image")
    private String shopImage;
    private String type = "sticker";
    private String code;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getVendorId() {
        return vendorId;
    }

    public void setVendorId(long vendorId) {
        this.vendorId = vendorId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getShopCountry() {
        return shopCountry;
    }

    public void setShopCountry(String shopCountry) {
        this.shopCountry = shopCountry;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
