package com.flashbid.flashcoinapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Loyalty extends ApiResponse{
    private int id;
    @SerializedName("company_name")
    private String companyName;
    private int points;
    private List<Loyalty> items;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public List<Loyalty> getItems() {
        return items;
    }

    public void setItems(List<Loyalty> items) {
        this.items = items;
    }
}
