package com.flashbid.flashcoinapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Contests extends ApiResponse implements Serializable {

    @SerializedName("room_id")
    private long roomId;
    @SerializedName("item_id")
    private long itemId;
    private String data;
    @SerializedName("final_bid")
    private long finalBid;
    @SerializedName("secondary_image")
    private String secondaryImage;
    @SerializedName("created_at")
    private String createdAt;
    private String description;
    @SerializedName("winner_id")
    private long winnerId;
    @SerializedName("initial_bid")
    private long initialBid;
    private String title;
    private String type;
    @SerializedName("bid_amount")
    private long bidAmount;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("qr_code")
    private String qrCode;
    private long id;
    private long stock;
    private String delivery;
    private String image;
    @SerializedName("market_value")
    private String marketValue;
    @SerializedName("ticket_sold")
    private long ticketSold;
    private String size;
    @SerializedName("buy_at_amount")
    private long buyAtAmount;
    @SerializedName("ticket_amount")
    private long ticketAmount;
    @SerializedName("ticket_total")
    private long ticketTotal;
    @SerializedName("item_worth")
    private long itemWorth;
    private String status;
    @SerializedName("full_description")
    private String fullDescription;
    @SerializedName("winner_instructions")
    private String winnerInstructions;
    @SerializedName("shop_title")
    private String shopTitle;
    @SerializedName("shop_country")
    private String shopCountry;
    @SerializedName("shop_image")
    private String shopImage;
    @SerializedName("other_images")
    private List<OtherImages> otherImages;
    @SerializedName("user_like")
    private boolean userLike;
    private double commission;
    private double tax;
    private double discount;

    public Contests(long id) {
        this.id = id;
    }

    public Contests() {
    }

    public final static String TYPE_BIG = "big";
    public final static String TYPE_SMALL = "small";

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getFinalBid() {
        return finalBid;
    }

    public void setFinalBid(long finalBid) {
        this.finalBid = finalBid;
    }

    public String getSecondaryImage() {
        return secondaryImage;
    }

    public void setSecondaryImage(String secondaryImage) {
        this.secondaryImage = secondaryImage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(long winnerId) {
        this.winnerId = winnerId;
    }

    public long getInitialBid() {
        return initialBid;
    }

    public void setInitialBid(long initialBid) {
        this.initialBid = initialBid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(long bidAmount) {
        this.bidAmount = bidAmount;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStock() {
        return stock;
    }

    public void setStock(long stock) {
        this.stock = stock;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(String marketValue) {
        this.marketValue = marketValue;
    }

    public long getTicketSold() {
        return ticketSold;
    }

    public void setTicketSold(long ticketSold) {
        this.ticketSold = ticketSold;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public long getBuyAtAmount() {
        return buyAtAmount;
    }

    public void setBuyAtAmount(long buyAtAmount) {
        this.buyAtAmount = buyAtAmount;
    }

    public long getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(long ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public long getTicketTotal() {
        return ticketTotal;
    }

    public void setTicketTotal(long ticketTotal) {
        this.ticketTotal = ticketTotal;
    }

    public long getItemWorth() {
        return itemWorth;
    }

    public void setItemWorth(long itemWorth) {
        this.itemWorth = itemWorth;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getWinnerInstructions() {
        return winnerInstructions;
    }

    public void setWinnerInstructions(String winnerInstructions) {
        this.winnerInstructions = winnerInstructions;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getShopCountry() {
        return shopCountry;
    }

    public void setShopCountry(String shopCountry) {
        this.shopCountry = shopCountry;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public List<OtherImages> getOtherImages() {
        return otherImages;
    }

    public void setOtherImages(List<OtherImages> otherImages) {
        this.otherImages = otherImages;
    }

    public boolean isUserLike() {
        return userLike;
    }

    public void setUserLike(boolean userLike) {
        this.userLike = userLike;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }
}
