package com.flashbid.flashcoinapp.models;

import com.google.gson.annotations.SerializedName;

public class TokenResponse extends ApiResponse {
    private int id;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("user_id")
    private int userId;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
