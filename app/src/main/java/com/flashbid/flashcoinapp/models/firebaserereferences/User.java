package com.flashbid.flashcoinapp.models.firebaserereferences;

public interface User extends UserBase {
    String EMAIL = "email";
    String PASSWORD = "password";
    String BALANCE = "balance";
    String PHONE_NUMBER = "phone_number";
    String ADDRESS = "address";
    String COUNTRY = "country";
    String STATE = "state";
    String CITY = "city";
    String POSTAL_CODE = "postal_code";
    String STATUS = "status";
    String MONEY_OWED = "money_owed";
    String DATA = "data";
    String AGE_GROUP = "age_group";
}
