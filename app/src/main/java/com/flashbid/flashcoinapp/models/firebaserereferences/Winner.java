package com.flashbid.flashcoinapp.models.firebaserereferences;

public interface Winner extends UserBase {
    String SCORE = "score";
    String TOTAL = "total";
    String TIME = "time";
}
