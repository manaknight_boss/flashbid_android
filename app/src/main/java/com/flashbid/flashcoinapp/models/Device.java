package com.flashbid.flashcoinapp.models;

import com.google.gson.annotations.SerializedName;

public class Device {
    @SerializedName("device_id")
    private String deviceId;
    @SerializedName("device_type")
    private String deviceType = "android";

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

}
