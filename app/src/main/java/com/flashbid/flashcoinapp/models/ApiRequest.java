package com.flashbid.flashcoinapp.models;

import java.util.ArrayList;
import java.util.List;

public class ApiRequest{

    private String action;
    private String parameter;
    private List<String> response = new ArrayList<>();
    private String lang = "sp";

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public List<String> getResponse() {
        return response;
    }

    public void setResponse(List<String> response) {
        this.response = response;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
