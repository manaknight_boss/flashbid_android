package com.flashbid.flashcoinapp.models;

public class OtherImages {

    private String image;

    public OtherImages(String image) {
        this.image = image;
    }

    public OtherImages() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
