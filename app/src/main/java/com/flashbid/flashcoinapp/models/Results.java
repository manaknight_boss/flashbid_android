package com.flashbid.flashcoinapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Results {

    private long id;
    private String message;
    @SerializedName("profile_image")
    private String profileImage;
    private String image;
    private String status;
    private String type;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("final_bid")
    private long finalBid;
    private List<Winners> winners;
    private String name;
    private boolean isWinnerListVisible;

    public static final String STATUS_COMPLETE = "complete";
    public static final String STATUS_LOST = "lost";

    public static final String SPECIAL_AUCTION = "a";
    public static final String LOTTERY = "l";
    public static final String AUCTION = "ta";
    public static final String TRIVIALITIES = "t";
    public static final String FOTP = "o";
    public static final String OTP = "r";

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Winners> getWinners() {
        return winners;
    }

    public void setWinners(List<Winners> winners) {
        this.winners = winners;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getFinalBid() {
        return finalBid;
    }

    public void setFinalBid(long finalBid) {
        this.finalBid = finalBid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isWinnerListVisible() {
        return isWinnerListVisible;
    }

    public void setWinnerListVisible(boolean winnerListVisible) {
        isWinnerListVisible = winnerListVisible;
    }
}
