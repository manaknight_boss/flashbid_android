package com.flashbid.flashcoinapp.models;

import com.google.gson.annotations.SerializedName;

public class ContestData {
    @SerializedName("start")
    private long startTime;


    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
