package com.flashbid.flashcoinapp.models;

import com.google.gson.annotations.SerializedName;

public class FlashPay extends ApiResponse{

    private long id;
    private String name;
    private double amount;
    @SerializedName("'pay_id'")
    private long payId;
    @SerializedName("shop_title")
    private String shopTitle;
    @SerializedName("shop_country")
    private String shopCountry;
    @SerializedName("shop_image")
    private String shopImage;
    private double tax;
    private double discount;
    private String status;
    private PayData data;
    private long points;
    private long pointsToBeUsed;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getPayId() {
        return payId;
    }

    public void setPayId(long payId) {
        this.payId = payId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getShopCountry() {
        return shopCountry;
    }

    public void setShopCountry(String shopCountry) {
        this.shopCountry = shopCountry;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PayData getData() {
        return data;
    }

    public void setData(PayData data) {
        this.data = data;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public long getPointsToBeUsed() {
        return pointsToBeUsed;
    }

    public void setPointsToBeUsed(long pointsToBeUsed) {
        this.pointsToBeUsed = pointsToBeUsed;
    }
}
