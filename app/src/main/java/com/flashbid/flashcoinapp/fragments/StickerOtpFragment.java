package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.FragmentOtpStickerBinding;
import com.flashbid.flashcoinapp.interfaces.DialogActionListener;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.ApiResponse;
import com.flashbid.flashcoinapp.models.Sticker;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StickerOtpFragment extends Fragment {
    private Context mContext;
    private HomeListener mCallback;
    private FragmentOtpStickerBinding mBinding;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private Sticker sticker;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentOtpStickerBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.popFragment();
            }
        });
        if(sticker == null) {
            mCallback.popFragment();
        }
        if (Utils.isNetworkAvailable(mContext)) {
            getData();
        } else {
            mCallback.showNoInternetErrorMessage();
        }
        return mBinding.getRoot();
    }

    private void getData() {
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setParameter(new Gson().toJson(sticker));
        apiRequest.setAction(Constants.ActionName.STICKER);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getStickerDetail(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<Sticker>() {
            @Override
            public void onResponse(@NonNull Call<Sticker> call, @NonNull Response<Sticker> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Sticker stickerResponse = response.body();
                    if (stickerResponse != null) {
                        if (stickerResponse.getStatusCode() == 403 || stickerResponse.getStatusCode() == 500 || stickerResponse.getStatusCode() == 404) {
                            DialogUtility.showDialog(mContext, stickerResponse.getMessage(), Constants.ERROR);
                        } else {
                            mBinding.progressBar.setVisibility(View.GONE);
                            mBinding.container.setVisibility(View.VISIBLE);
                            String code = StickerOtpFragment.this.sticker.getCode();
                            StickerOtpFragment.this.sticker = stickerResponse;
                            StickerOtpFragment.this.sticker.setCode(code);
                            if (isAdded()) {
                                setData();
                            }
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Sticker> call, @NonNull Throwable t) {
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void setData() {
        if (!TextUtils.isEmpty(sticker.getShopImage())) {
            Picasso.get().load(sticker.getShopImage()).placeholder(R.mipmap.drawable_xxxhdpi_icon).into(mBinding.shopImage);
        }
        mBinding.shopName.setText(sticker.getShopTitle());
        mBinding.shopCountry.setText(sticker.getShopCountry());
        Utils.setHTMLToTextView("<b>Menudo Flash</b>($"+sticker.getAmount()+")",mBinding.name);
        mBinding.info.setText("Canjear "+sticker.getAmount()+" FP");
        mBinding.exchangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double walletBalance = mSharedPreferenceUtility.getUserWalletBalance();
                if (walletBalance >= sticker.getAmount()) {
                    if (Utils.isNetworkAvailable(mContext)) {
                        buySticker();
                    } else {
                        mCallback.showNoInternetErrorMessage();
                    }
                } else {
                    DialogUtility.showDialog(mContext, mContext.getString(R.string.insufficient_balance), Constants.ERROR);
                }
            }
        });
    }

    private void buySticker() {
        mCallback.showScreenProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.buySticker(mSharedPreferenceUtility.getAccessToken(), sticker).enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                mCallback.hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    ApiResponse apiResponse = response.body();
                    if(apiResponse != null) {
                        if (apiResponse.getStatusCode() == 403 || apiResponse.getStatusCode() == 404) {
                            DialogUtility.showDialog(mContext, apiResponse.getMessage(), Constants.ERROR);
                        } else {
                            DialogUtility.showDialog(mContext, getString(R.string.item_purchased), Constants.SUCCESS, new DialogActionListener() {
                                @Override
                                public void onClick() {
                                    mCallback.popFragment();
                                }
                            });
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                mCallback.hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    public void setItem(Sticker sticker) {
        this.sticker = sticker;
//        String data[] = sticker.getCode().split(":");
//        String code = data[0];
//        this.sticker.setCode(code);
    }
}
