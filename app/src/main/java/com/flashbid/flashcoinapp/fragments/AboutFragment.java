package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.flashbid.flashcoinapp.databinding.FragmentAboutBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.utils.Utils;

public class AboutFragment extends Fragment {
    private FragmentAboutBinding mBinding;
    private Context mContext;
    private HomeListener mCallback;

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentAboutBinding.inflate(inflater,container,false);

        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.popFragment();
            }
        });

        mBinding.webView.getSettings().setJavaScriptEnabled(true);
        mBinding.webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            public void onPageFinished(WebView view, String url) {
                mBinding.progressBar.setVisibility(View.GONE);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(mContext, "" + description, Toast.LENGTH_SHORT).show();
            }

        });
        if(Utils.isNetworkAvailable(mContext)) {
            mBinding.webView.loadUrl("https://www.flashbidrd.com/");
        } else {
            mCallback.showNoInternetErrorMessage();
        }
        return mBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

}
