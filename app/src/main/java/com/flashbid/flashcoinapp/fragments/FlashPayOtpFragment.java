package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.FragmentFlashPayOtpBinding;
import com.flashbid.flashcoinapp.interfaces.DialogActionListener;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.ApiResponse;
import com.flashbid.flashcoinapp.models.FlashPay;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlashPayOtpFragment extends Fragment {
    private Context mContext;
    private HomeListener mCallback;
    private FragmentFlashPayOtpBinding mBinding;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private FlashPay flashPay;
    private long itemId;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentFlashPayOtpBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        if (savedInstanceState != null) {
            itemId = savedInstanceState.getLong(Constants.ITEM_ID);
        }
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.popFragment();
            }
        });
        if (Utils.isNetworkAvailable(mContext)) {
            getData();
        } else {
            mCallback.showNoInternetErrorMessage();
        }
        return mBinding.getRoot();
    }

    private void getData() {
        ApiRequest apiRequest = new ApiRequest();
        final FlashPay flashPay = new FlashPay();
        flashPay.setId(itemId);
        apiRequest.setParameter(new Gson().toJson(flashPay));
        apiRequest.setAction(Constants.ActionName.FLASH_PAY);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getFlashPayDetail(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<FlashPay>() {
            @Override
            public void onResponse(@NonNull Call<FlashPay> call, @NonNull Response<FlashPay> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    FlashPay flashPayResponse = response.body();
                    if (flashPayResponse != null) {
                        if (flashPayResponse.getStatusCode() == 403 || flashPayResponse.getStatusCode() == 500 || flashPayResponse.getStatusCode() == 404) {
                            DialogUtility.showDialog(mContext, flashPayResponse.getMessage(), Constants.ERROR);
                        } else {
                            mBinding.progressBar.setVisibility(View.GONE);
                            mBinding.container.setVisibility(View.VISIBLE);
                            FlashPayOtpFragment.this.flashPay = flashPayResponse;
                            if (isAdded()) {
                                setData();
                            }
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<FlashPay> call, @NonNull Throwable t) {
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void setData() {
        if (!TextUtils.isEmpty(flashPay.getShopImage())) {
            Picasso.get().load(flashPay.getShopImage()).placeholder(R.mipmap.drawable_xxxhdpi_icon).into(mBinding.shopImage);
        }
        mBinding.shopName.setText(flashPay.getShopTitle());
        mBinding.shopCountry.setText(flashPay.getShopCountry());
        mBinding.payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double walletBalance = mSharedPreferenceUtility.getUserWalletBalance();
                if (walletBalance >= flashPay.getAmount()) {
                    if (Utils.isNetworkAvailable(mContext)) {
                        payBill();
                    } else {
                        mCallback.showNoInternetErrorMessage();
                    }
                } else {
                    DialogUtility.showDialog(mContext, mContext.getString(R.string.insufficient_balance), Constants.ERROR);
                }
            }
        });
        mBinding.name.setText(flashPay.getName());
        setAmount();
        if(flashPay.getData().getThreshold() > 0) {
            if(flashPay.getData().getRedeem() > 0) {
                flashPay.setPointsToBeUsed(flashPay.getData().getRedeem());
                setAmount();
            } else if(flashPay.getPoints() > 0) {
                final long pointsToBeUsed;
                if(flashPay.getPoints() > flashPay.getData().getThreshold()) {
                    pointsToBeUsed = flashPay.getData().getThreshold();
                } else {
                    pointsToBeUsed = flashPay.getPoints();
                }
                DialogUtility.showConfirmDialog(mContext, mContext.getString(R.string.points_to_exchange, pointsToBeUsed), DialogUtility.YES_NO_OPTION, new DialogActionListener() {
                    @Override
                    public void onClick() {
                        flashPay.setPointsToBeUsed(pointsToBeUsed);
                        setAmount();
                    }
                });
            }
        }
    }

    private void setAmount() {
        mBinding.amount.setText(""+(int)getTotalBill());
        mBinding.subTotal.setText(Utils.roundOffTo2DecPlaces(flashPay.getAmount()));
        if((flashPay.getDiscount() + flashPay.getPointsToBeUsed()) != 0) {
            mBinding.discount.setText("-"+Utils.roundOffTo2DecPlaces(flashPay.getDiscount() + flashPay.getPointsToBeUsed()));
        } else {
            mBinding.discount.setText("0.00");
        }
        mBinding.taxes.setText(Utils.roundOffTo2DecPlaces(flashPay.getTax()));
        mBinding.total.setText(Utils.roundOffTo2DecPlaces(getTotalBill()));
    }

    private double getTotalBill() {
        return flashPay.getAmount() + flashPay.getTax() - flashPay.getDiscount() - flashPay.getPointsToBeUsed();
    }

    private void payBill() {
        mCallback.showScreenProgress();
        flashPay.setAmount((long)getTotalBill());
        flashPay.setPoints(flashPay.getPointsToBeUsed());
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.payBill(mSharedPreferenceUtility.getAccessToken(), flashPay).enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                mCallback.hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    DialogUtility.showDialog(mContext, getString(R.string.item_purchased), Constants.SUCCESS, new DialogActionListener() {
                        @Override
                        public void onClick() {
                            mCallback.popFragment();
                        }
                    });
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                mCallback.hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    public void setItemId(long id) {
        itemId = id;
    }
}
