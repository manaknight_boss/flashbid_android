package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.flashbid.flashcoinapp.databinding.FragmentUserOtpBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.UserDetail;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class UserOtpFragment extends Fragment {
    private Context mContext;
    private HomeListener mCallback ;
    private FragmentUserOtpBinding mBinding;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentUserOtpBinding.inflate(inflater, container, false);
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.popFragment();
            }
        });
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,Utils.getScreenWidth(mContext));
        mBinding.image.setLayoutParams(params);
        if(getArguments() != null) {
            UserDetail user = (UserDetail)getArguments().getSerializable(Constants.ActionName.PROFILE);
            if(user != null) {
                mBinding.email.setText(user.getEmail());
                mBinding.name.setText(user.getFirstName()+" "+user.getLastName());
                Picasso.get().load(user.getProfileImage()).into(mBinding.image, new Callback() {
                    @Override
                    public void onSuccess() {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                mCallback.popFragment();
            }
        } else {
            mCallback.popFragment();
        }
        return mBinding.getRoot();
    }

    public static UserOtpFragment newInstance(UserDetail user) {
        UserOtpFragment userOtpFragment = new UserOtpFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.ActionName.PROFILE, user);
        userOtpFragment.setArguments(args);
        return userOtpFragment;
    }
}
