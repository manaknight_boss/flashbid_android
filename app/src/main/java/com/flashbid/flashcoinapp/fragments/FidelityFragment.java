package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.R;

import com.flashbid.flashcoinapp.adapters.FidelityAdapter;
import com.flashbid.flashcoinapp.databinding.FragmentFidelityBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.Loyalty;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FidelityFragment extends Fragment {
    private FragmentFidelityBinding mBinding;
    private HomeListener mCallback;
    private Context mContext;
    private FidelityAdapter mAdapter;
    private List<Loyalty> mList = new ArrayList<>();
    private SharedPreferenceUtility mSharedPreferenceUtility;

    public static FidelityFragment newInstance() {
        return new FidelityFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentFidelityBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        setAdapter();
        if(Utils.isNetworkAvailable(mContext)) {
            getLoyaltyList();
        } else {
            mCallback.showNoInternetErrorMessage();
        }

        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.popFragment();
            }
        });

        return mBinding.getRoot();
    }

    private void getLoyaltyList() {
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setAction(Constants.ActionName.LOYALTY);
        apiRequest.setParameter(Constants.NO_PARAMETERS);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getLoyalty(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<Loyalty>() {
            @Override
            public void onResponse(@NonNull Call<Loyalty> call, @NonNull Response<Loyalty> response) {
                mBinding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.code() == 200) {
                    Loyalty loyaltyResponse = response.body();
                    if(loyaltyResponse != null) {
                        if (loyaltyResponse.getStatusCode() == 403 || loyaltyResponse.getStatusCode() == 500) {
                            DialogUtility.showDialog(mContext, loyaltyResponse.getMessage(), Constants.ERROR);
                        } else {
                            if(loyaltyResponse.getItems() != null && loyaltyResponse.getItems().size() > 0) {
                                mAdapter.setList(loyaltyResponse.getItems());
                            } else {
                                mBinding.noFidelity.setVisibility(View.VISIBLE);
                                mBinding.recyclerView.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Loyalty> call, @NonNull Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void setAdapter() {
        mAdapter = new FidelityAdapter(mContext, mList, mCallback);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }


}
