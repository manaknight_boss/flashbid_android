package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.adapters.HistoryAdapter;
import com.flashbid.flashcoinapp.databinding.FragmentHistoryBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.History;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment {
    private FragmentHistoryBinding mBinding;
    private HomeListener mCallback;
    private Context mContext;
    private HistoryAdapter mAdapter;
    private List<History> mList = new ArrayList<>();
    private boolean isAlreadyCalled;
    private SharedPreferenceUtility mSharedPreferenceUtility;


    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentHistoryBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        setAdapter();
        if(Utils.isNetworkAvailable(mContext)) {
            getHistoryList(0);
        } else {
            mCallback.showNoInternetErrorMessage();
        }

        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.popFragment();
            }
        });
        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - Constants.LOAD_MORE_ITEMS_AFTER;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && mList.size() > 0) {
                        int id = mList.get(mList.size()-1).getId();
                        isAlreadyCalled = true;
                        getHistoryList(id);
                    }
                }
            }
        });

        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utils.isNetworkAvailable(mContext)) {
                    getHistoryList(0);
                } else {
                    hideSwipeRefresh();
                    mCallback.showNoInternetErrorMessage();
                }
            }
        });

        return mBinding.getRoot();
    }

    public void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    private void getHistoryList(final int id) {
        ApiRequest apiRequest = new ApiRequest();
        History history = new History();
        history.setId(id);
        history.setLimit(Constants.HISTORY_LIMIT);
        apiRequest.setParameter(new Gson().toJson(history));
        apiRequest.setAction(Constants.ActionName.HISTORY);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getHistory(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<History>() {
            @Override
            public void onResponse(@NonNull Call<History> call, @NonNull Response<History> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    History historyResponse = response.body();
                    if(historyResponse != null) {
                        if (historyResponse.getStatusCode() == 403 || historyResponse.getStatusCode() == 500) {
                            DialogUtility.showDialog(mContext, historyResponse.getMessage(), Constants.ERROR);
                        } else {
                            updateHistory(historyResponse,id != 0);
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<History> call, @NonNull Throwable t) {
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void setAdapter() {
        mAdapter = new HistoryAdapter(mContext, mList, mCallback);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void updateHistory(History history, boolean append) {
        if(!append) {
            mList = history.getItems();
        } else {
            mList.addAll(history.getItems());
        }
        mAdapter.setList(mList);
        if (mList.size() == 0) {
            mBinding.noHistory.setVisibility(View.VISIBLE);
            mBinding.recyclerView.setVisibility(View.GONE);
        } else {
            mBinding.noHistory.setVisibility(View.GONE);
            mBinding.recyclerView.setVisibility(View.VISIBLE);
        }
        if(history.getItems().size() == 0) {
            setLoadMoreProgress(false);
        } else {
            setLoadMoreProgress(true);
        }
        mBinding.progressBar.setVisibility(View.GONE);
        isAlreadyCalled = false;
        hideSwipeRefresh();
    }

    private void setLoadMoreProgress(boolean isProgressRequired) {
        if (isProgressRequired) {
            mAdapter.setProgress(true);
        } else {
            mAdapter.setProgress(false);
        }
    }

}
