package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.adapters.ImageViewPagerAdapter;
import com.flashbid.flashcoinapp.databinding.FragmentDetailBinding;
import com.flashbid.flashcoinapp.interfaces.DialogActionListener;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.ApiResponse;
import com.flashbid.flashcoinapp.models.ContestData;
import com.flashbid.flashcoinapp.models.Contests;
import com.flashbid.flashcoinapp.models.OtherImages;
import com.flashbid.flashcoinapp.models.Results;
import com.flashbid.flashcoinapp.models.firebaserereferences.Contest;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFragment extends Fragment {

    private Context mContext;
    private HomeListener mCallback;
    private FragmentDetailBinding mBinding;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private ImageViewPagerAdapter adapter;
    private int dotsCount = 0;
    private ImageView[] dots;
    private long itemId;
    private Contests contest;
    private boolean isOtherItem;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentDetailBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, Utils.getScreenWidth(mContext));
        mBinding.viewPager.setLayoutParams(params);
        if (savedInstanceState != null) {
            itemId = savedInstanceState.getLong(Constants.ITEM_ID);
        }
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.popFragment();
            }
        });
        if (Utils.isNetworkAvailable(mContext)) {
            getData();
            setupFirebaseForContests();
        } else {
            mCallback.showNoInternetErrorMessage();
        }
        return mBinding.getRoot();
    }

    private void getData() {
        ApiRequest apiRequest = new ApiRequest();
        final Contests contest = new Contests();
        contest.setId(itemId);
        apiRequest.setParameter(new Gson().toJson(contest));
        if(isOtherItem) {
            apiRequest.setAction(Constants.ActionName.OTHER_ITEM);
        } else {
            apiRequest.setAction(Constants.ActionName.ITEM);
        }
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getItemDetail(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<Contests>() {
            @Override
            public void onResponse(@NonNull Call<Contests> call, @NonNull Response<Contests> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Contests contestResponse = response.body();
                    if (contestResponse != null) {
                        if (contestResponse.getStatusCode() == 403 || contestResponse.getStatusCode() == 500) {
                            DialogUtility.showDialog(mContext, contestResponse.getMessage(), Constants.ERROR);
                        } else {
                            mBinding.progressBar.setVisibility(View.GONE);
                            mBinding.container.setVisibility(View.VISIBLE);
                            DetailFragment.this.contest = contestResponse;
                            if (isAdded()) {
                                setData();
                            }
                        }
                    } else {
                        DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Contests> call, @NonNull Throwable t) {
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void setupFirebaseForContests() {
        final DatabaseReference resultsReference = mCallback.getFirebaseDatabase().child(Constants.FirebaseReferences.ALL_ITEMS).child("1");
        resultsReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Object object = dataSnapshot.getValue();
                if (object instanceof Map) {
                    Map<String, Object> contestMap = (Map) object;
                    long id = (long)contestMap.get(Contest.ID);
                    if(id == itemId) {
                        Contests contest = ContestFragment.getContestFromMap(object);
                        if (contest != null) {
                            DetailFragment.this.contest.setTicketSold(contest.getTicketSold());
                            DetailFragment.this.contest.setStatus(contest.getStatus());
                            DetailFragment.this.contest.setTicketTotal(contest.getTicketTotal());
                            setProgress();
                            checkStatus();
                        } else {
                            mBinding.progressHorizontal.setProgress(100);
                            mBinding.actionButton.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    private void setData() {
        if (!TextUtils.isEmpty(contest.getShopImage())) {
            Picasso.get().load(contest.getShopImage()).placeholder(R.mipmap.drawable_xxxhdpi_icon).into(mBinding.shopImage);
        }
        mBinding.shopName.setText(contest.getShopTitle());
        mBinding.shopCountry.setText(contest.getShopCountry());
        mBinding.itemName.setText(contest.getTitle());
        mBinding.itemPrice.setText("($" + contest.getMarketValue() + ")");
        mBinding.itemDescription.setText(contest.getDescription());
        mBinding.informationDetail.setText(contest.getFullDescription());
        changeColorAndType();
        setProgress();
        checkStatus();
        updateLikeUI(contest.isUserLike());
        mBinding.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                likeClick();
            }
        });

        List<OtherImages> images = contest.getOtherImages();
        if(TextUtils.isEmpty(contest.getSecondaryImage())) {
            images.add(0, new OtherImages(contest.getImage()));
        } else {
            images.add(0, new OtherImages(contest.getSecondaryImage()));
        }
        adapter = new ImageViewPagerAdapter(mContext, images);
        mBinding.viewPager.setAdapter(adapter);
        setUiPageViewController();
        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.mipmap.unselected));
                }
                dots[position].setImageDrawable(getResources().getDrawable(R.mipmap.selected));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mBinding.actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double walletBalance = mSharedPreferenceUtility.getUserWalletBalance();
                if (contest.getType().equalsIgnoreCase(Results.LOTTERY) || contest.getType().equalsIgnoreCase(Results.SPECIAL_AUCTION)) {
                    if (walletBalance >= contest.getTicketAmount()) {
                        if (Utils.isNetworkAvailable(mContext)) {
                            getTicket();
                        } else {
                            mCallback.showNoInternetErrorMessage();
                        }
                    } else {
                        DialogUtility.showDialog(mContext, mContext.getString(R.string.insufficient_balance), Constants.ERROR);
                    }
                } else if (contest.getType().equalsIgnoreCase(Results.FOTP)) {
                    if (walletBalance >= contest.getBuyAtAmount()) {
                        if (Utils.isNetworkAvailable(mContext)) {
                            buyItem();
                        } else {
                            mCallback.showNoInternetErrorMessage();
                        }
                    } else {
                        DialogUtility.showDialog(mContext, mContext.getString(R.string.insufficient_balance), Constants.ERROR);
                    }
                } else if (contest.getType().equalsIgnoreCase(Results.OTP)) {
                    if (walletBalance >= getItemTotal()) {
                        if (Utils.isNetworkAvailable(mContext)) {
                            buyItem();
                        } else {
                            mCallback.showNoInternetErrorMessage();
                        }
                    } else {
                        DialogUtility.showDialog(mContext, mContext.getString(R.string.insufficient_balance), Constants.ERROR);
                    }
                }
            }
        });
    }

    private void likeClick() {
        contest.setUserLike(!contest.isUserLike());
        updateLikeUI(contest.isUserLike());
        postsLike();
    }

    private void postsLike() {
        ApiRequest apiRequest = new ApiRequest();
        Contests contest = new Contests();
        contest.setItemId(itemId);
        contest.setType(this.contest.getType());
        apiRequest.setParameter(new Gson().toJson(contest));
        apiRequest.setAction(Constants.ActionName.LIKE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.putLike(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                mCallback.hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {

                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                mCallback.hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void buyItem() {
        mCallback.showScreenProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.buyItem(mSharedPreferenceUtility.getAccessToken(), contest).enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                mCallback.hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    DialogUtility.showDialog(mContext, getString(R.string.item_purchased), Constants.SUCCESS, new DialogActionListener() {
                        @Override
                        public void onClick() {
                            mCallback.popFragment();
                        }
                    });
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                mCallback.hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }


    private void updateLikeUI(boolean userLiked) {
        if (userLiked) {
            mBinding.like.setImageResource(R.mipmap.black_heart);
        } else {
            mBinding.like.setImageResource(R.mipmap.heart);
        }
    }

    private void checkStatus() {
        removeAllBottomLayouts();
        if (contest.getStatus().equalsIgnoreCase(Constants.STATUS_COMPLETE) || contest.getStatus().equalsIgnoreCase(Constants.STATUS_LOST) || contest.getStatus().equalsIgnoreCase(Constants.STATUS_CLAIMED)) {
            mBinding.actionLayout.setVisibility(View.VISIBLE);
            mBinding.progressHorizontal.setProgress(100);
            mBinding.actionButton.setVisibility(View.GONE);
        } else if (contest.getStatus().equalsIgnoreCase(Constants.STATUS_QUEUE)) {
            mBinding.itemInQueue.setVisibility(View.VISIBLE);
        } else if (contest.getStatus().equalsIgnoreCase(Constants.STATUS_FLASH)) {
            mBinding.enterAuction.setVisibility(View.VISIBLE);
            mBinding.enterAuction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(mContext, "Todo", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            mBinding.actionLayout.setVisibility(View.VISIBLE);
        }
    }

    private void removeAllBottomLayouts() {
        mBinding.actionLayout.setVisibility(View.GONE);
        mBinding.enterAuction.setVisibility(View.GONE);
        mBinding.itemInQueue.setVisibility(View.GONE);
    }

    private void setUiPageViewController() {
        dotsCount = adapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(mContext);
            dots[i].setImageDrawable(getResources().getDrawable(R.mipmap.unselected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(11, 7, 11, 7);
            mBinding.dotsLayout.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.mipmap.selected));
    }

    private void getTicket() {
        mCallback.showScreenProgress();
        ApiRequest apiRequest = new ApiRequest();
        Contests contest = new Contests();
        contest.setId(itemId);
        apiRequest.setParameter(new Gson().toJson(contest));
        apiRequest.setAction(Constants.ActionName.TICKET);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getTicket(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                mCallback.hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    DialogUtility.showDialog(mContext, getString(R.string.ticket_purchased), Constants.SUCCESS, new DialogActionListener() {
                        @Override
                        public void onClick() {
                            mCallback.popFragment();
                        }
                    });
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                mCallback.hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void changeColorAndType() {
        if (contest.getType().equalsIgnoreCase(Results.SPECIAL_AUCTION)) {
            mBinding.actionIcon.setImageResource(R.mipmap.special_auction);
            mBinding.progressHorizontal.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar_blue));
            mBinding.title.setText(mContext.getString(R.string.special_auction));
            mBinding.info.setText(mContext.getString(R.string.info_auction));
            mBinding.points.setText("" + contest.getTicketAmount());
        } else if (contest.getType().equalsIgnoreCase(Results.LOTTERY)) {
            mBinding.progressHorizontal.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar_red));
            mBinding.actionIcon.setImageResource(R.mipmap.lottery);
            mBinding.title.setText(mContext.getString(R.string.lottery));
            mBinding.points.setText("" + contest.getTicketAmount());
            Utils.setHTMLToTextView(mContext.getString(R.string.info_tickets_amount, contest.getTicketAmount()), mBinding.info);
        } else if (contest.getType().equalsIgnoreCase(Results.TRIVIALITIES)) {
            mBinding.actionIcon.setImageResource(R.mipmap.trivialities);
            mBinding.progressHorizontal.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar_light_blue));
            mBinding.title.setText(mContext.getString(R.string.trivialities));
        } else if (contest.getType().equalsIgnoreCase(Results.FOTP)) {
            mBinding.actionIcon.setImageResource(R.mipmap.item_icon);
            mBinding.title.setText(mContext.getString(R.string.store));
            mBinding.info.setText(mContext.getString(R.string.info_item, contest.getStock() > 0 ? mContext.getString(R.string.available) : mContext.getString(R.string.exhausted)));
            mBinding.points.setText("" + contest.getBuyAtAmount());
        } else if (contest.getType().equalsIgnoreCase(Results.OTP)) {
            mBinding.actionIcon.setImageResource(R.mipmap.item_icon);
            mBinding.title.setText(mContext.getString(R.string.store));
            mBinding.info.setText(mContext.getString(R.string.info_item, contest.getStock() > 0 ? mContext.getString(R.string.available) : mContext.getString(R.string.exhausted)));
            mBinding.points.setText("" + (int) getItemTotal());
            mBinding.itemPrice.setText("($" + (int) getItemTotal() + ")");
        } else {
            mBinding.actionIcon.setImageResource(R.mipmap.ribbon_purple);
            mBinding.progressHorizontal.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar_purple));
            mBinding.title.setText(mContext.getString(R.string.auction));
        }
    }

    private double getItemTotal() {
        return contest.getBuyAtAmount() + contest.getCommission() - contest.getDiscount() + contest.getTax();
    }

    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    public void setItemId(long id) {
        this.itemId = id;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putLong(Constants.ITEM_ID, itemId);
        super.onSaveInstanceState(outState);
    }

    private void setProgress() {
        int progress = 0;
        if (contest.getType().equalsIgnoreCase(Results.FOTP) || contest.getType().equalsIgnoreCase(Results.OTP)) {
            mBinding.progressLayout.setVisibility(View.GONE);
            return;
        }
        if (contest.getType().equalsIgnoreCase(Results.SPECIAL_AUCTION) || contest.getType().equalsIgnoreCase(Results.LOTTERY)) {
            progress = (int) (((double) contest.getTicketSold() / contest.getTicketTotal()) * 100);
        } else {
            ContestData contestData = new Gson().fromJson(contest.getData(), ContestData.class);
            long currentMillis = Calendar.getInstance().getTimeInMillis();
            long diffMs = contestData.getStartTime() - currentMillis;
            long diffSec = diffMs / 1000;
            long min = diffSec / 60;
            long hours = min / 60;
            if (hours < 24) {
                progress = 100 - (int) ((double) hours / 24 * 100);
            }
        }
        if (progress < 10) {
            progress = 10;
        }
        if (progress >= 100) {
            mBinding.actionButton.setVisibility(View.GONE);
        }
        mBinding.progressHorizontal.setProgress(progress);
        if (contest.getType().equalsIgnoreCase(Results.LOTTERY)) {
            mBinding.progressStatus.setText(mContext.getString(R.string.tickets_sold_percentage, progress));
        } else if (contest.getType().equalsIgnoreCase(Results.SPECIAL_AUCTION)) {
            mBinding.progressStatus.setText(mContext.getString(R.string.auction_starting_percentage, progress));
        }
    }

    public void setItemId(long id, boolean isOtherItem) {
        setItemId(id);
        this.isOtherItem = isOtherItem;
    }
}
