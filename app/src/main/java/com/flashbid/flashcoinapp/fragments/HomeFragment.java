package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.adapters.ViewPagerAdapter;
import com.flashbid.flashcoinapp.databinding.FragmentHomeBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;

public class HomeFragment extends Fragment {
    private FragmentHomeBinding mBinding;
    private HomeListener mCallBack;
    private Context mContext;
    private ViewPagerAdapter viewPagerAdapter;
    private ContestFragment contestFragment;
    private BaseFragment baseFragment2;
    private ServicesFragment servicesFragment;
    private ResultFragment resultFragment;


    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentHomeBinding.inflate(inflater, container, false);
        mBinding.drawerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallBack.onDrawerMenuClick();
            }
        });
        setViewPager();
        mBinding.scanFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openScanSelectionDialog();
            }
        });
        return mBinding.getRoot();
    }

    private void setViewPager() {
        if (viewPagerAdapter == null) {
            viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
            contestFragment = new ContestFragment();
            baseFragment2 = new BaseFragment();
            servicesFragment = new ServicesFragment();
            resultFragment = new ResultFragment();
            viewPagerAdapter.addFragment(contestFragment, mContext.getString(R.string.competitions));
            viewPagerAdapter.addFragment(baseFragment2, mContext.getString(R.string.stores));
            viewPagerAdapter.addFragment(servicesFragment, mContext.getString(R.string.services));
            viewPagerAdapter.addFragment(resultFragment, mContext.getString(R.string.results));
        }
        mBinding.viewpager.setOffscreenPageLimit(1);
        mBinding.viewpager.setAdapter(viewPagerAdapter);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        changeTabText();
    }

    private void changeTabText() {
        for (int i = 0; i < mBinding.tabs.getTabCount(); i++) {
            TextView textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.tab_custom_home, null);
            mBinding.tabs.getTabAt(i).setCustomView(textView);
        }
    }

}
