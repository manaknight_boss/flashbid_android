package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.flashbid.flashcoinapp.databinding.FragmentQrCodeBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QRCodeFragment extends Fragment {

    private Context mContext;
    private FragmentQrCodeBinding mBinding;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private HomeListener mCallBack;

    public static QRCodeFragment newInstance() {
        return new QRCodeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentQrCodeBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.popFragment();
            }
        });
        generateQRCode();
        return mBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (HomeListener) context;
    }

    private void generateQRCode() {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(mSharedPreferenceUtility.getQrCode(), BarcodeFormat.QR_CODE, Constants.QR_CODE_SIZE, Constants.QR_CODE_SIZE);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            mBinding.myQrCode.setImageBitmap(bmp);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
}

