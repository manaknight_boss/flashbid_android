package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.FragmentEditProfileBinding;
import com.flashbid.flashcoinapp.interfaces.DialogActionListener;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.ApiRequest;
import com.flashbid.flashcoinapp.models.TokenResponse;
import com.flashbid.flashcoinapp.models.UserDetail;
import com.flashbid.flashcoinapp.networks.ApiEndpointInterface;
import com.flashbid.flashcoinapp.networks.ApiService;
import com.flashbid.flashcoinapp.sharedpreference.SharedPreferenceUtility;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.DialogUtility;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileFragment extends Fragment {
    private FragmentEditProfileBinding mBinding;
    private HomeListener mCallback;
    private Context mContext;
    private UserDetail mUserDetail = new UserDetail();
    private List<String> mSpinnerList = new ArrayList<>();
    private int ageGroup = 0;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private String imageUrl;

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentEditProfileBinding.inflate(inflater, container, false);
        mUserDetail = mCallback.getMyProfile();
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        mSpinnerList.add(mContext.getString(R.string.dominician_republic));
        ArrayAdapter<String> postTypeAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_item, mSpinnerList);
        mBinding.spinner.setAdapter(postTypeAdapter);
        init();
        setData();
        return mBinding.getRoot();
    }

    private void init() {
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.popFragment();
            }
        });
        mBinding.editPhotoText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.EDIT_PROFILE_IMAGE, 1, true);
            }
        });
        mBinding.maleRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.femaleRadioButton.setChecked(false);
            }
        });
        mBinding.femaleRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.maleRadioButton.setChecked(false);
            }
        });
        mBinding.age1To10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ageGroup = Constants.AGE_GROUP_1_10;
                setAgeGroup(mBinding.age1To10);
            }
        });
        mBinding.age11To20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ageGroup = Constants.AGE_GROUP_11_20;
                setAgeGroup(mBinding.age11To20);
            }
        });
        mBinding.age21To30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ageGroup = Constants.AGE_GROUP_21_30;
                setAgeGroup(mBinding.age21To30);
            }
        });
        mBinding.age31To40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ageGroup = Constants.AGE_GROUP_31_40;
                setAgeGroup(mBinding.age31To40);
            }
        });
        mBinding.age41To50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ageGroup = Constants.AGE_GROUP_41_50;
                setAgeGroup(mBinding.age41To50);
            }
        });
        mBinding.age50Plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ageGroup = Constants.AGE_GROUP_50_PLUS;
                setAgeGroup(mBinding.age50Plus);
            }
        });
        mBinding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataToServer();
            }
        });
    }

    private void sendDataToServer() {
        final String email, password, firstName, lastName, phone, address, state, country, city, postalCode, gender;
        email = mBinding.inputEmail.getText().toString().trim();
        password = mBinding.inputPassword.getText().toString().trim();
        firstName = mBinding.inputFirstName.getText().toString().trim();
        lastName = mBinding.inputSurnames.getText().toString().trim();
        phone = mBinding.inputPhone.getText().toString().trim();
        address = mBinding.inputAddress.getText().toString();
        state = mBinding.inputState.getText().toString();
        city = mBinding.inputCity.getText().toString();
        postalCode = mBinding.inputPostalCode.getText().toString().trim();
        country = mBinding.spinner.getSelectedItem().toString();
        if(mBinding.maleRadioButton.isChecked()) {
            gender = Constants.MALE;
        } else if(mBinding.femaleRadioButton.isChecked()) {
            gender = Constants.FEMALE;
        } else {
            gender = null;
        }
        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mBinding.scrollView.scrollTo(0, mBinding.inputLayoutEmail.getTop());
            mBinding.inputLayoutEmail.setError(getString(R.string.email_validation));
            return;
        } else {
            mBinding.inputLayoutEmail.setError(null);
        }
        if (!TextUtils.isEmpty(password)) {
            if (!Utils.isPasswordValidMethod(password)) {
                mBinding.scrollView.scrollTo(0, mBinding.inputLayoutPassword.getTop());
                mBinding.inputLayoutPassword.setError(getString(R.string.password_validation));
                return;
            } else {
                mBinding.inputLayoutPassword.setError(null);
            }
        } else {
            mBinding.inputLayoutPassword.setError(null);
        }

        if (TextUtils.isEmpty(firstName)) {
            mBinding.scrollView.scrollTo(0, mBinding.inputLayoutFirstName.getTop());
            mBinding.inputLayoutFirstName.setError(getString(R.string.first_name_validation));
            return;
        } else {
            mBinding.inputLayoutFirstName.setError(null);
        }

        if (TextUtils.isEmpty(lastName)) {
            mBinding.scrollView.scrollTo(0, mBinding.inputLayoutSurnames.getTop());
            mBinding.inputLayoutSurnames.setError(getString(R.string.last_name_validation));
            return;
        } else {
            mBinding.inputLayoutSurnames.setError(null);
        }

        if (TextUtils.isEmpty(phone)) {
            mBinding.scrollView.scrollTo(0, mBinding.inputLayoutPhone.getTop());
            mBinding.inputLayoutPhone.setError(getString(R.string.phone_validation));
            return;
        } else {
            mBinding.inputLayoutPhone.setError(null);
        }

        if (TextUtils.isEmpty(address)) {
            mBinding.scrollView.scrollTo(0, mBinding.inputLayoutAddress.getTop());
            mBinding.inputLayoutAddress.setError(getString(R.string.address_validation));
            return;
        } else {
            mBinding.inputLayoutAddress.setError(null);
        }

        if (TextUtils.isEmpty(state)) {
            mBinding.scrollView.scrollTo(0, mBinding.inputLayoutProvince.getTop());
            mBinding.inputLayoutProvince.setError(getString(R.string.state_validation));
            return;
        } else {
            mBinding.inputLayoutProvince.setError(null);
        }

        if (TextUtils.isEmpty(city)) {
            mBinding.inputLayoutCity.setError(getString(R.string.city_validation));
            return;
        } else {
            mBinding.inputLayoutCity.setError(null);
        }

        if (TextUtils.isEmpty(postalCode)) {
            mBinding.inputLayoutPostalCode.setError(getString(R.string.postal_code_validation));
            return;
        } else {
            mBinding.inputLayoutPostalCode.setError(null);
        }
        UserDetail userDetail = new UserDetail();
        userDetail.setEmail(email);
        userDetail.setPassword(password);
        userDetail.setFirstName(firstName);
        userDetail.setLastName(lastName);
        userDetail.setPhoneNumber(phone);
        userDetail.setAddress(address);
        userDetail.setState(state);
        userDetail.setCity(city);
        userDetail.setCountry(country);
        userDetail.setAgeGroup(ageGroup);
        userDetail.setPostalCode(postalCode);
        userDetail.setGender(gender);
        userDetail.setProfileImage(imageUrl);
        if(Utils.isNetworkAvailable(mContext)) {
            userDetailRequest(userDetail);
        } else {
            mCallback.showNoInternetErrorMessage();
        }
    }

    private void userDetailRequest(UserDetail userDetail) {
        mCallback.showScreenProgress();
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setParameter(new Gson().toJson(userDetail));
        apiRequest.setAction(Constants.ActionName.PROFILE_EDIT);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.updateProfileDetail(mSharedPreferenceUtility.getAccessToken(), apiRequest).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call, @NonNull Response<TokenResponse> response) {
                mCallback.hideScreenProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    TokenResponse tokenResponse = response.body();
                    if (tokenResponse != null && tokenResponse.getStatusCode() == 403) {
                        DialogUtility.showDialog(mContext, tokenResponse.getMessage(), Constants.ERROR);
                    } else {
                        DialogUtility.showDialog(mContext, mContext.getString(R.string.profile_updated), Constants.SUCCESS, new DialogActionListener() {
                            @Override
                            public void onClick() {
                                mCallback.popFragment();
                            }
                        });
                    }
                } else {
                    DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call, @NonNull Throwable t) {
                mCallback.hideScreenProgress();
                DialogUtility.showDialog(mContext, getString(R.string.something_went_wrong), Constants.ERROR);
            }
        });
    }

    private void setAgeGroup(AppCompatRadioButton ageGroup) {
        mBinding.age1To10.setChecked(false);
        mBinding.age11To20.setChecked(false);
        mBinding.age21To30.setChecked(false);
        mBinding.age31To40.setChecked(false);
        mBinding.age41To50.setChecked(false);
        mBinding.age50Plus.setChecked(false);
        if(ageGroup != null) {
            ageGroup.setChecked(true);
        }
    }

    private void setData() {
        imageUrl = mUserDetail.getProfileImage();
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.get().load(imageUrl).placeholder(R.mipmap.profile).into(mBinding.profileImage);
        } else {
            mBinding.profileImage.setImageResource(R.mipmap.profile);
        }
        mBinding.inputEmail.setText(mUserDetail.getEmail());
        mBinding.inputFirstName.setText(mUserDetail.getFirstName());
        mBinding.inputSurnames.setText(mUserDetail.getLastName());
        if(TextUtils.isEmpty(mUserDetail.getPhoneNumber())) {
            mBinding.inputPhone.setText(null);
        } else {
            mBinding.inputPhone.setText(mUserDetail.getPhoneNumber());
        }
        mBinding.inputAddress.setText(mUserDetail.getAddress());
        mBinding.inputState.setText(mUserDetail.getState());
        mBinding.inputCity.setText(mUserDetail.getCity());
        mBinding.inputPostalCode.setText(mUserDetail.getPostalCode());
        String gender = mUserDetail.getGender();
        ageGroup = mUserDetail.getAgeGroup();
        if (!TextUtils.isEmpty(gender)) {
            if (gender.equalsIgnoreCase(Constants.MALE)) {
                mBinding.maleRadioButton.setChecked(true);
                mBinding.femaleRadioButton.setChecked(false);
            } else {
                mBinding.maleRadioButton.setChecked(false);
                mBinding.femaleRadioButton.setChecked(true);
            }
        } else {
            mBinding.maleRadioButton.setChecked(false);
            mBinding.femaleRadioButton.setChecked(false);
        }
        if (ageGroup == Constants.AGE_GROUP_1_10) {
            setAgeGroup(mBinding.age1To10);
        } else if (ageGroup == Constants.AGE_GROUP_11_20) {
            setAgeGroup(mBinding.age11To20);
        } else if (ageGroup == Constants.AGE_GROUP_21_30) {
            setAgeGroup(mBinding.age21To30);
        } else if (ageGroup == Constants.AGE_GROUP_31_40) {
            setAgeGroup(mBinding.age31To40);
        } else if (ageGroup == Constants.AGE_GROUP_41_50) {
            setAgeGroup(mBinding.age41To50);
        } else if (ageGroup == Constants.AGE_GROUP_50_PLUS){
            setAgeGroup(mBinding.age50Plus);
        } else {
            setAgeGroup(null);
        }
    }

    public void setPhoto(String compressedDisplayPic) {
        File fLocal = new File(compressedDisplayPic);
        try {
            Picasso.get().load(fLocal).into(mBinding.profileImage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setPhotoURL(String newUrl) {
        imageUrl = newUrl;
    }
}
