package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.adapters.ServicesAdapter;
import com.flashbid.flashcoinapp.databinding.FragmentServicesBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.Services;

import java.util.ArrayList;
import java.util.List;

public class ServicesFragment extends Fragment {
    private FragmentServicesBinding mBinding;
    private HomeListener mCallback;
    private Context mContext;
    private List<Services> mList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    public static ServicesFragment newInstance() {
        return new ServicesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentServicesBinding.inflate(inflater, container, false);
        if(mList.size() == 0) {
            addData();
        }
        setAdapter();
        return mBinding.getRoot();
    }

    private void addData() {
        mList.add(new Services(getString(R.string.refill),R.mipmap.phoneshare,getString(R.string.recharge_your_minutes)));
        mList.add(new Services(getString(R.string.surveys),R.mipmap.poll,getString(R.string.earn_points_for_your_opinion)));
        mList.add(new Services(getString(R.string.fidelity),R.mipmap.fidelidad,getString(R.string.check_your_membership)));
    }

    private void setAdapter() {
        ServicesAdapter mAdapter = new ServicesAdapter(mContext, mList, mCallback);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }
}
