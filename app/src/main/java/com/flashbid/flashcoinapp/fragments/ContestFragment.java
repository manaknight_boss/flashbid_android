package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.adapters.ContestAdapter;
import com.flashbid.flashcoinapp.databinding.FragmentContestBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.Contests;
import com.flashbid.flashcoinapp.models.firebaserereferences.Contest;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ContestFragment extends Fragment {
    private FragmentContestBinding mBinding;
    private HomeListener mCallback;
    private Context mContext;
    private ContestAdapter mAdapter;
    private List<Contests> mList = new ArrayList<>();
    private Timer timer;
    int REFRESH_DELAY = 1000 * 60 * 10;
    int REFRESH_RELOAD_TIME = 1000 * 60 * 10;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentContestBinding.inflate(inflater, container, false);
        if (mList.size() > 0) {
            mBinding.progressBar.setVisibility(View.GONE);
        }
        setAdapter();
        return mBinding.getRoot();
    }

    private void startRefreshTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (Utils.isNetworkAvailable(mContext)) {
                    setupFirebaseForContests();
                }
            }
        }, REFRESH_DELAY, REFRESH_RELOAD_TIME);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Utils.isNetworkAvailable(mContext)) {
            setupFirebaseForContests();
        } else {
            mCallback.showNoInternetErrorMessage();
        }
        startRefreshTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopTimer();
    }

    private void stopTimer() {
        if(timer!=null) {
            timer.cancel();
            timer.purge();
        }
    }

    private void setupFirebaseForContests() {
        final DatabaseReference resultsReference = mCallback.getFirebaseDatabase().child(Constants.FirebaseReferences.ALL_ITEMS).child("1");
        resultsReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Object object = dataSnapshot.getValue();
                if (object instanceof Map) {
                    Contests contest = getContestFromMap(object);
                    if (contest != null) {
                        mList.add(contest);
                        mAdapter.setList(mList);
                    }
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Object object = dataSnapshot.getValue();
                if (object instanceof Map) {
                    Contests contest = getContestFromMap(object);
                    if (contest != null) {
                        int index = indexOfContestInList(contest);
                        if (index != -1) {
                            mList.set(index, contest);
                            mAdapter.setList(mList);
                        }
                    } else {
                        Map<String, Object> contestMap = (Map) object;
                        long id = (long) contestMap.get(Contest.ID);
                        int index = indexOfContestInList(new Contests(id));
                        if (index != -1) {
                            mList.remove(index);
                            mAdapter.setList(mList);
                        }
                    }
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    Object object = dataSnapshot.getValue();
                    if (object instanceof List) {
                        List<Contests> tempList = new ArrayList<>();
                        List contests = (List) object;
                        for (Object contestItem : contests) {
                            if (contestItem instanceof Map) {
                                Contests contest = getContestFromMap(contestItem);
                                if (contest != null) {
                                    tempList.add(contest);
                                }
                            }
                        }
                        mList.clear();
                        mList.addAll(tempList);
                        mAdapter.setList(mList);
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        resultsReference.addListenerForSingleValueEvent(eventListener);
    }

    private int indexOfContestInList(Contests newContest) {
        for (int i = 0; i < mList.size(); i++) {
            Contests contest = mList.get(i);
            if (contest.getId() == newContest.getId()) {
                return i;
            }
        }
        return -1;
    }

    public static Contests getContestFromMap(Object contestItem) {
        Map<String, Object> contestMap = (Map) contestItem;
        Contests contest = new Contests();
        contest.setStatus((String) contestMap.get(Contest.STATUS));
        if (contest.getStatus().equalsIgnoreCase(Constants.STATUS_COMPLETE) || contest.getStatus().equalsIgnoreCase(Constants.STATUS_LOST) || contest.getStatus().equalsIgnoreCase(Constants.STATUS_CLAIMED)) {
            return null;
        }
        contest.setId((long) contestMap.get(Contest.ID));
        contest.setBidAmount((long) contestMap.get(Contest.BID_AMOUNT));
        contest.setBuyAtAmount((long) contestMap.get(Contest.BUY_AT_AMOUNT));
        contest.setCreatedAt((String) contestMap.get(Contest.CREATED_AT));
        contest.setData((String) contestMap.get(Contest.DATA));
        contest.setDelivery((String) contestMap.get(Contest.DELIVERY));
        contest.setDescription((String) contestMap.get(Contest.DESCRIPTION));
        contest.setFinalBid((long) contestMap.get(Contest.FINAL_BID));
        contest.setImage((String) contestMap.get(Contest.IMAGE));
        contest.setInitialBid((long) contestMap.get(Contest.INITIAL_BID));
        contest.setItemWorth((long) contestMap.get(Contest.ITEM_WORTH));
        contest.setMarketValue((String) contestMap.get(Contest.MARKET_VALUE));
        contest.setQrCode((String) contestMap.get(Contest.QR_CODE));
        contest.setRoomId((long) contestMap.get(Contest.ROOM_ID));
        contest.setSecondaryImage((String) contestMap.get(Contest.SECONDARY_IMAGE));
        contest.setSize((String) contestMap.get(Contest.SIZE));
        contest.setStock((long) contestMap.get(Contest.STOCK));
        contest.setTicketAmount((long) contestMap.get(Contest.TICKET_AMOUNT));
        contest.setTicketSold((long) contestMap.get(Contest.TICKET_SOLD));
        contest.setTicketTotal((long) contestMap.get(Contest.TICKET_TOTAL));
        contest.setTitle((String) contestMap.get(Contest.TITLE));
        contest.setType((String) contestMap.get(Contest.TYPE));
        contest.setWinnerId((long) contestMap.get(Contest.WINNER_ID));
        contest.setUpdatedAt((String) contestMap.get(Contest.UPDATED_AT));
        return contest;
    }

    private void setAdapter() {
        mAdapter = new ContestAdapter(mContext, mList, mCallback);
        mBinding.recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mBinding.recyclerView.setAdapter(mAdapter);
    }
}
