package com.flashbid.flashcoinapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.adapters.ResultAdapter;
import com.flashbid.flashcoinapp.databinding.FragmentResultBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.Results;
import com.flashbid.flashcoinapp.models.Winners;
import com.flashbid.flashcoinapp.models.firebaserereferences.Winner;
import com.flashbid.flashcoinapp.utils.Constants;
import com.flashbid.flashcoinapp.utils.Utils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.flashbid.flashcoinapp.models.firebaserereferences.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ResultFragment extends Fragment {
    private FragmentResultBinding mBinding;
    private HomeListener mCallback;
    private Context mContext;
    private ResultAdapter mAdapter;
    private List<Results> mList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (HomeListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentResultBinding.inflate(inflater, container, false);
        if (mList.size() > 0) {
            mBinding.progressBar.setVisibility(View.GONE);
        } else {
            if (Utils.isNetworkAvailable(mContext)) {
                setupFirebaseForResults();
            } else {
                mCallback.showNoInternetErrorMessage();
            }
        }
        setAdapter();


        return mBinding.getRoot();
    }

    private void setupFirebaseForResults() {
        final DatabaseReference resultsReference = mCallback.getFirebaseDatabase().child(Constants.FirebaseReferences.RESULTS).child("1");
        resultsReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Object object = dataSnapshot.getValue();
                if (object instanceof Map) {
                    Results result = getResultFromMap(object);
                    int index = indexOfResultInList(result);
                    if(index != -1) {
                        mList.set(index,result);
                        mAdapter.setList(mList);
                    }
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    Object object = dataSnapshot.getValue();
                    if (object instanceof List) {
                        List results = (List) object;
                        for (Object resultItem : results) {
                            if (resultItem instanceof Map) {
                                mList.add(getResultFromMap(resultItem));
                            }
                        }
                        mAdapter.setList(mList);
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                    Log.d("msg", "msg");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        resultsReference.addListenerForSingleValueEvent(eventListener);
    }

    private int indexOfResultInList(Results newResult) {
        for(int i = 0; i < mList.size(); i++) {
            Results result = mList.get(i);
            if(result.getId() == newResult.getId()) {
                return i;
            }
        }
        return -1;
    }

    private Results getResultFromMap(Object resultItem) {
        Map<String, Object> resultMap = (Map) resultItem;
        Results result = new Results();
        result.setId((long) resultMap.get(Result.ID));
        result.setProfileImage((String) resultMap.get(Result.PROFILE_IMAGE));
        result.setImage((String) resultMap.get(Result.IMAGE));
        result.setStatus((String) resultMap.get(Result.STATUS));
        result.setType((String) resultMap.get(Result.TYPE));
        result.setUpdatedAt((String) resultMap.get(Result.UPDATED_AT));
        result.setUpdatedAt(Utils.formatTimeToMMMDDYYYY(result.getUpdatedAt()));
        result.setFinalBid((long) resultMap.get(Result.FINAL_BID));
        List<Winners> winners = new ArrayList<>();
        if(result.getStatus().equalsIgnoreCase(Results.STATUS_LOST)) {
            result.setMessage(getString(R.string.message_no_winner));
        } else {
            result.setMessage((String) resultMap.get(Result.TITLE));
            if(result.getType().equalsIgnoreCase(Results.TRIVIALITIES)) {
                List winnersFromFirebase = (List) resultMap.get(Result.WINNERS);
                if (winnersFromFirebase != null) {
                    for (Object winnerItem : winnersFromFirebase) {
                        if (winnerItem instanceof Map) {
                            Map<String, Object> winnerMap = (Map) winnerItem;
                            Winners winner = new Winners();
                            winner.setId((long) winnerMap.get(Winner.ID));
                            winner.setFirstName((String) winnerMap.get(Winner.FIRST_NAME));
                            winner.setLastName((String) winnerMap.get(Winner.LAST_NAME));
                            winner.setProfileImage((String) winnerMap.get(Winner.PROFILE_IMAGE));
                            winner.setScore((long) winnerMap.get(Winner.SCORE));
                            winner.setTotal((long) winnerMap.get(Winner.TOTAL));
                            winner.setTime((long) winnerMap.get(Winner.TIME));
                            if(winner.getScore() == winner.getTotal()) {
                                winner.setMessage(getString(R.string.message_perfect_score));
                            } else {
                                winner.setMessage(getString(R.string.message_next_winner_score,winner.getScore(),winner.getTotal()));
                            }
                            winners.add(winner);
                        }
                    }
                }
            } else {
                Winners winner = new Winners();
                winner.setId(0);
                winner.setFirstName((String) resultMap.get(Winner.FIRST_NAME));
                winner.setLastName((String) resultMap.get(Winner.LAST_NAME));
                winner.setProfileImage((String) resultMap.get(Winner.PROFILE_IMAGE));
                if(result.getType().equalsIgnoreCase(Results.LOTTERY)) {
                    winner.setMessage(getString(R.string.message_congratulation_to_winner));
                } else {
                    winner.setMessage(getString(R.string.message_congratulation_to_highest_winner,result.getFinalBid()));
                }
                winners.add(winner);
            }
        }
        result.setWinners(winners);
        return result;
    }

    private void setAdapter() {
        mAdapter = new ResultAdapter(mContext, mList, mCallback);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }
}
