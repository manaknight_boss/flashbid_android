package com.flashbid.flashcoinapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.databinding.FragmentBaseBinding;


public class BaseFragment extends Fragment {
private FragmentBaseBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentBaseBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

}
