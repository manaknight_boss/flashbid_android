package com.flashbid.flashcoinapp.utils;

import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.Html;
import android.view.Display;
import android.view.WindowManager;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class Utils {

    public static boolean isPasswordValidMethod(String password) {
        String expression = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()=_+:;.,/?'\"|`~-]).{8,20})";
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
    }

    public static String roundOffTo2DecPlaces(double value) {
        return String.format(Locale.US, "%.2f", value);
    }


    public static String formatTimeToMMMDDYYYY(String updatedAt) {
        try {
            SimpleDateFormat currentSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date updatedDate = currentSimpleDateFormat.parse(updatedAt);
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy", Locale.US);
            return sdf.format(updatedDate);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static void setHTMLToTextView(String source, TextView into) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            into.setText(Html.fromHtml(source, Html.FROM_HTML_MODE_COMPACT));
        } else {
            into.setText(Html.fromHtml(source));
        }
    }

    public static int dpToPx(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    public static int getScreenWidth(Context c) {
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

}
