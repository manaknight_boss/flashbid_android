package com.flashbid.flashcoinapp.utils;

import android.content.Context;

import java.io.File;

public class ImageCache {
    private File cacheDir;
    public ImageCache(Context context) {
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), ".flashbid_image_cache");
        } else {
            cacheDir = context.getCacheDir();
        }
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
    }
    public File getCacheDirectory() {
        return cacheDir;
    }
}
