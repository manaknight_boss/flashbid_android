package com.flashbid.flashcoinapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.flashbid.flashcoinapp.R;


public class ProgressBarDialog {
    private Dialog popDialog;
    private Context mContext;
    private boolean isProgressBarVisible = true;

    public ProgressBarDialog(Context context) {
        this.mContext = context;
    }

    public void showProgress() {
        if (isProgressBarVisible) {
            isProgressBarVisible = false;
            popDialog = new Dialog(mContext);
            popDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            popDialog.setContentView(R.layout.progressbar);
            popDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            popDialog.setCancelable(false);

            popDialog.show();
        }
    }

    public void hideProgress() {
        if (popDialog != null) {
            isProgressBarVisible = true;
            popDialog.dismiss();
            popDialog = null;
        }
    }
}
