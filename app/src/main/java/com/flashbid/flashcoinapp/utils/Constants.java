package com.flashbid.flashcoinapp.utils;

public interface Constants {

    int ERROR = 1;
    int SUCCESS = 0;
    String MALE = "m";
    String FEMALE = "f";
    int EDIT_PROFILE_IMAGE = 1;
    String PACKAGE = "package";
    String CROPPED_IMAGE_EXTRA = "cropped_image_extra";
    int  COMPRESS_80 = 80;
    int REQUEST_CROPPER = 20;
    int NO_COMPRESSION = 100;

    int SERVICE_TYPE_REFILL = 0;
    int SERVICE_TYPE_SURVEY = 1;
    int SERVICE_TYPE_FIDELITY = 2;

    int AGE_GROUP_1_10 = 1;
    int AGE_GROUP_11_20 = 2;
    int AGE_GROUP_21_30 = 3;
    int AGE_GROUP_31_40 = 4;
    int AGE_GROUP_41_50 = 5;
    int AGE_GROUP_50_PLUS = 6;

    int MAX_PROFILE_PHOTO_WIDTH = 250;
    int MAX_PROFILE_PHOTO_HEIGHT = 250;

    String KEY = "AKIAIOAE7XB65XOIDRBA";
    String SECRET = "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";
    String IMAGE = "image";
    String S3_IMAGE_BUCKET = "images.flashbid";
    String IMAGE_INITIAL = "IMG_";
    String S3_IMAGE_BASE_URL = "https://s3.amazonaws.com/";
    String NO_PARAMETERS = "{}";

    int SPECIAL_AUCTION = 1;
    int LOTTERY = 2;
    int FP = 3;
    int INFO = 4;
    int STORE = 5;
    int AUCTION = 7;
    int TRIVIALITIES = 8;
    int LOYALTY = 9;
    int LOAD_MORE_ITEMS_AFTER = 10;
    int HISTORY_LIMIT = 20;
    int QR_CODE_SIZE = 500;
    long UNDER_MAINTENANCE = 1;
    String STATUS_ACTIVE = "active";
    String STATUS_LOST = "lost";
    String STATUS_COMPLETE = "complete";
    String STATUS_CLAIMED = "claimed";
    String STATUS_QUEUE = "queue";
    String STATUS_FLASH = "flash";

    String ITEM_ID = "item_id";

    interface FirebaseReferences {
        String TERMS_AND_CONDITIONS = "terms";
        String USER = "user";
        String MAINTENANCE = "maintenance";
        String RESULTS = "results";
        String ALL_ITEMS = "allItems";
    }

    interface ActionName {
        String LOGOUT = "logout";
        String DEVICE = "device";
        String HISTORY = "history";
        String PROFILE = "profile";
        String PROFILE_EDIT = "profileEdit";
        String PROFILE_IMAGE = "profileImage";
        String LOYALTY = "loyalty";
        String ITEM = "item";
        String OTHER_ITEM = "otherItem";
        String STICKER = "sticker";
        String FLASH_PAY = "flashpay";
        String LIKE = "like";
        String TICKET = "ticket";
        String CHECK_CODE = "checkCode";
    }


    interface ScanType {
        String USER = "user";
        String STICKER = "sticker";
        String FOTP = "fotp";
        String OTP = "otp";
        String PAY = "pay";
        String AUCTION = "auction";
        String LOTTERY = "lottery";
    }

}
