package com.flashbid.flashcoinapp.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.DialogConfirmBinding;
import com.flashbid.flashcoinapp.databinding.DialogProgressBinding;
import com.flashbid.flashcoinapp.interfaces.DialogActionListener;


public class DialogUtility {

    public static final int YES_NO_OPTION = 1;

    public static void showDialog(Context mContext, String message, int type) {
        showDialog(mContext, message, type, null);
    }

    public static void showDialog(Context mContext, String message, int type, final DialogActionListener listener) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.error_dialog);
        TextView showMessage = dialog.findViewById(R.id.message);
        RelativeLayout okButton = dialog.findViewById(R.id.ok_button);

        if (type == Constants.ERROR) {
            okButton.setBackground(mContext.getResources().getDrawable(R.drawable.round_corner_red));
        } else {
            okButton.setBackground(mContext.getResources().getDrawable(R.drawable.round_corner));
        }

        showMessage.setText(message);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showConfirmDialog(Context mContext, String message, int type, final DialogActionListener listener) {
        DialogConfirmBinding dialogBinding = DialogConfirmBinding.inflate(LayoutInflater.from(mContext));
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(dialogBinding.getRoot());
        dialog.show();
        if(type == YES_NO_OPTION) {
            dialogBinding.negativeButtonText.setText("NO");
            dialogBinding.positiveButtonText.setText("YES");
        }
        dialogBinding.message.setText(message);
        dialogBinding.positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick();
                }
                dialog.dismiss();
            }
        });
        dialogBinding.negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public static void showProgressDialog(Context mContext, String message) {
        DialogProgressBinding dialogBinding = DialogProgressBinding.inflate(LayoutInflater.from(mContext));
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(dialogBinding.getRoot());
        dialogBinding.message.setText(message);
        dialog.show();
    }



    public static void displayPermissionMessageDialog(final Context mContext, String message) {
        AlertDialog mAlertDialog = new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.permissions))
                .setMessage(message)
                .setPositiveButton(mContext.getString(R.string.settings), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts(Constants.PACKAGE, mContext.getPackageName(), null);
                        intent.setData(uri);
                        mContext.startActivity(intent);
                    }
                })

                .create();
        mAlertDialog.show();
    }
}
