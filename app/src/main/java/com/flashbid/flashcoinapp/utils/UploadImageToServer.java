package com.flashbid.flashcoinapp.utils;

import android.os.AsyncTask;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;

public class UploadImageToServer extends AsyncTask<Void,Void,Void> {

    private AmazonS3Client s3Client;
    private String imagePath;
    private String imageName;

    public UploadImageToServer(AmazonS3Client s3Client, String imagePath, String imageName) {
        this.s3Client = s3Client;
        this.imagePath = imagePath;
        this.imageName = imageName;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if(s3Client.doesBucketExist(Constants.S3_IMAGE_BUCKET)) {
            PutObjectRequest putObjectRequest = new PutObjectRequest(Constants.S3_IMAGE_BUCKET, imageName, new File(imagePath));
            s3Client.putObject(putObjectRequest);
        }
        return null;
    }
}
