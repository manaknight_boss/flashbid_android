package com.flashbid.flashcoinapp.interfaces;

public interface DialogActionListener {
    void onClick();
}
