package com.flashbid.flashcoinapp.interfaces;

import android.support.v4.app.Fragment;

import com.ncapdevi.fragnav.FragNavTransactionOptions;

public interface FragmentNavigation
{
    void pushFragment(Fragment fragment);
    void pushFragment(Fragment fragment, FragNavTransactionOptions transactionOptions);
}
