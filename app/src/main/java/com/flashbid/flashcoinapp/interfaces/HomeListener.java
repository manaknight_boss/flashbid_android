package com.flashbid.flashcoinapp.interfaces;

import com.flashbid.flashcoinapp.models.Contests;
import com.flashbid.flashcoinapp.models.UserDetail;
import com.google.firebase.database.DatabaseReference;

public interface HomeListener extends DrawerMenuListener {

    UserDetail getMyProfile();

    void openGalleryFor(int editProfileImage, int i, boolean captureRequired);

    void showScreenProgress();

    void hideScreenProgress();

    void showNoInternetErrorMessage();

    DatabaseReference getFirebaseDatabase();

    void openServiceFragment(int serviceType);

    void openScanSelectionDialog();

    void openItemDetail(Contests contest);

}
