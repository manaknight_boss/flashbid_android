package com.flashbid.flashcoinapp.interfaces;


public interface DrawerMenuListener extends OnBackPressedListener
{
    void onDrawerMenuClick();
}
