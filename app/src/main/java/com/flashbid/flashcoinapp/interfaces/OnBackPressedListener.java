package com.flashbid.flashcoinapp.interfaces;

public interface OnBackPressedListener {
    void popFragment();
}
