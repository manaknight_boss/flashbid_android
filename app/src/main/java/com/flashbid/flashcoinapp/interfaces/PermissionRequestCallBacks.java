package com.flashbid.flashcoinapp.interfaces;

public interface PermissionRequestCallBacks {
    void onPermissionsGranted();
    void onPermissionsDenied();
}
