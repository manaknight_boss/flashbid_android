package com.flashbid.flashcoinapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.HistoryItemBinding;
import com.flashbid.flashcoinapp.databinding.LoadMoreProgressBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.History;
import com.flashbid.flashcoinapp.utils.Constants;

import java.util.List;


public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    private List<History> mList;
    private final HomeListener mCallback;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;

    public HistoryAdapter(Context mContext, List<History> mList, HomeListener mCallback) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new HistoryAdapter.ViewHolderProgress(mBindingProgress);
        } else {
            HistoryItemBinding mBindingItem = HistoryItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new HistoryAdapter.ViewHolderItem(mBindingItem);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HistoryAdapter.ViewHolderProgress) {
            viewHolderProgress((HistoryAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof HistoryAdapter.ViewHolderItem) {
            viewHolderItem((HistoryAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(HistoryAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final HistoryAdapter.ViewHolderItem holder, int position) {
        final History history = mList.get(position);
        final int type = history.getType();
        String title = null;
        if(type == Constants.SPECIAL_AUCTION) {
            title = mContext.getString(R.string.special_auction);
            holder.itemView.icon.setImageResource(R.mipmap.special_auction);
        } else if(type == Constants.LOTTERY) {
            title = mContext.getString(R.string.lottery);
            holder.itemView.icon.setImageResource(R.mipmap.lottery);
        } else if(type == Constants.FP) {
            title = mContext.getString(R.string.fp);
            holder.itemView.icon.setImageResource(R.mipmap.fp);
        } else if(type == Constants.INFO) {
            title = mContext.getString(R.string.info);
            holder.itemView.icon.setImageResource(R.mipmap.info);
        } else if(type == Constants.STORE) {
            title = mContext.getString(R.string.store);
            holder.itemView.icon.setImageResource(R.mipmap.store);
        } else if(type == Constants.AUCTION) {
            title = mContext.getString(R.string.auction);
            holder.itemView.icon.setImageResource(R.mipmap.auction);
        } else if(type == Constants.TRIVIALITIES) {
            title = mContext.getString(R.string.trivialities);
            holder.itemView.icon.setImageResource(R.mipmap.trivialities);
        } else {
            title = mContext.getString(R.string.fidelity);
            holder.itemView.icon.setImageResource(R.mipmap.group_6);
        }
        holder.itemView.title.setText(title);
        holder.itemView.message.setText(history.getMessage());
    }

    @Override
    public int getItemCount() {
        return mList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<History> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        HistoryItemBinding itemView;
        private ViewHolderItem(HistoryItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;
        private ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}