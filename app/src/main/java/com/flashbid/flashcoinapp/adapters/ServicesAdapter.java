package com.flashbid.flashcoinapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.databinding.ServicesItemBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.Services;


import java.util.List;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Services> mList;
    private HomeListener mCallback;

    public ServicesAdapter(Context mContext, List<Services> mWinnerList, HomeListener mCallback) {
        this.mContext = mContext;
        this.mList = mWinnerList;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ServicesItemBinding mBinding = ServicesItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderItem holder,final int position) {
        final Services service = mList.get(position);
        holder.itemView.icon.setImageResource(service.getImage());
        holder.itemView.title.setText(service.getServiceName());
        holder.itemView.message.setText(service.getMessage());
        holder.itemView.servicesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openServiceFragment(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        ServicesItemBinding itemView;
        private ViewHolderItem(ServicesItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

