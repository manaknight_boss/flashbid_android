package com.flashbid.flashcoinapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.flashbid.flashcoinapp.databinding.ViewPagerImageBinding;
import com.flashbid.flashcoinapp.models.OtherImages;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import java.util.List;


public class ImageViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<OtherImages> images;

    public ImageViewPagerAdapter(Context mContext, List<OtherImages> images) {
        this.mContext = mContext;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final ViewPagerImageBinding mBinding = ViewPagerImageBinding.inflate(LayoutInflater.from(mContext));
        OtherImages image = images.get(position);
        if(!TextUtils.isEmpty(image.getImage())) {
            Picasso.get().load(image.getImage()).noPlaceholder().into(mBinding.image, new Callback() {
                @Override
                public void onSuccess() {
                    mBinding.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    mBinding.progressBar.setVisibility(View.GONE);
                }
            });
        }
        container.addView(mBinding.getRoot());
        return mBinding.getRoot();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }


}



