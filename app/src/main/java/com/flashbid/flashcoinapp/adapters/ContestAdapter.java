package com.flashbid.flashcoinapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.ContestsItemBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.ContestData;
import com.flashbid.flashcoinapp.models.Contests;
import com.flashbid.flashcoinapp.models.Results;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;


public class ContestAdapter extends RecyclerView.Adapter<ContestAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Contests> mList;
    private HomeListener mCallback;

    public ContestAdapter(Context mContext, List<Contests> mList, HomeListener mCallback) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ContestsItemBinding mBinding = ContestsItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderItem holder, int position) {
        final Contests contest = mList.get(position);
        changeCardColor(holder,contest);
        setProgress(holder,contest);
        if(!TextUtils.isEmpty(contest.getImage())) {
            if(contest.getSize().equalsIgnoreCase(Contests.TYPE_BIG)) {
                Picasso.get().load(contest.getImage()).placeholder(R.mipmap.rectangle).into(holder.itemView.image);
            } else {
                Picasso.get().load(contest.getImage()).placeholder(R.mipmap.square).into(holder.itemView.image);
            }
        } else {
            holder.itemView.image.setImageResource(R.mipmap.ic_launcher);
        }
        holder.itemView.title.setText(contest.getTitle());
        holder.itemView.description.setText(contest.getDescription());
        holder.itemView.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openItemDetail(contest);
            }
        });
    }

    private void setProgress(ViewHolderItem holder, Contests contest) {
        int progress = 0;
        boolean showBanner = false;
        if(contest.getType().equalsIgnoreCase(Results.SPECIAL_AUCTION) || contest.getType().equalsIgnoreCase(Results.LOTTERY)) {
            progress = (int)(((double)contest.getTicketSold()/contest.getTicketTotal())*100);
            if(progress > 85) {
                showBanner = true;
            }
        } else {
            ContestData contestData = new Gson().fromJson(contest.getData(),ContestData.class);
            long currentMillis = Calendar.getInstance().getTimeInMillis();
            long diffMs = contestData.getStartTime() - currentMillis;
            long diffSec = diffMs / 1000;
            long min = diffSec / 60;
            long hours = min / 60;
            if(hours < 24) {
                progress = 100 - (int)((double)hours/24*100);
            }
            if(hours <= 3) {
                showBanner = true;
            }
        }
        if(progress < 10) {
            progress = 10;
        }
        holder.itemView.progressHorizontal.setProgress(progress);
        if(showBanner) {
            holder.itemView.headingLayout.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.headingLayout.setVisibility(View.GONE);
        }
    }

    private void changeCardColor(ViewHolderItem holder, Contests contest) {
        if(contest.getType().equalsIgnoreCase(Results.SPECIAL_AUCTION)) {
            holder.itemView.headingLayout.setBackgroundColor(mContext.getResources().getColor(R.color.card_blue));
            holder.itemView.timeIcon.setImageResource(R.mipmap.time_blue);
            holder.itemView.actionIcon.setImageResource(R.mipmap.group);
            holder.itemView.progressHorizontal.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar_blue));
            holder.itemView.heading.setText(mContext.getString(R.string.few_remaining));
        } else if(contest.getType().equalsIgnoreCase(Results.LOTTERY)) {
            holder.itemView.headingLayout.setBackgroundColor(mContext.getResources().getColor(R.color.card_red));
            holder.itemView.timeIcon.setImageResource(R.mipmap.time_red);
            holder.itemView.actionIcon.setImageResource(R.mipmap.ribbon_red);
            holder.itemView.progressHorizontal.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar_red));
            holder.itemView.heading.setText(mContext.getString(R.string.few_remaining));
        } else if(contest.getType().equalsIgnoreCase(Results.TRIVIALITIES)) {
            holder.itemView.headingLayout.setBackgroundColor(mContext.getResources().getColor(R.color.card_light_blue));
            holder.itemView.timeIcon.setImageResource(R.mipmap.time_light_blue);
            holder.itemView.actionIcon.setImageResource(R.mipmap.trivia_icon);
            holder.itemView.progressHorizontal.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar_light_blue));
            holder.itemView.heading.setText(mContext.getString(R.string.start_soon));
        } else {
            holder.itemView.headingLayout.setBackgroundColor(mContext.getResources().getColor(R.color.card_purple));
            holder.itemView.timeIcon.setImageResource(R.mipmap.time_purple);
            holder.itemView.actionIcon.setImageResource(R.mipmap.ribbon_purple);
            holder.itemView.progressHorizontal.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar_purple));
            holder.itemView.heading.setText(mContext.getString(R.string.almost_there));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Contests> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {
        ContestsItemBinding itemView;

        private ViewHolderItem(ContestsItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

