package com.flashbid.flashcoinapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.WinnerItemBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.Winners;
import com.squareup.picasso.Picasso;

import java.util.List;


public class WinnerAdapter extends RecyclerView.Adapter<WinnerAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Winners> mWinnerList;
    private HomeListener mCallback;

    public WinnerAdapter(Context mContext, List<Winners> mWinnerList, HomeListener mCallback) {
        this.mContext = mContext;
        this.mWinnerList = mWinnerList;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        WinnerItemBinding mBinding = WinnerItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderItem holder, int position) {
        final Winners winner = mWinnerList.get(position);
        if(!TextUtils.isEmpty(winner.getProfileImage())) {
            Picasso.get().load(winner.getProfileImage()).placeholder(R.mipmap.profile).into(holder.itemView.icon);
        }
        holder.itemView.title.setText(winner.getFirstName() + " " + winner.getLastName().charAt(0)+".");
        holder.itemView.message.setText(winner.getMessage());
        if(winner.getTime() > 0) {
            holder.itemView.time.setVisibility(View.VISIBLE);
            holder.itemView.time.setText(mContext.getString(R.string.time_in_sec, winner.getTime()));
        } else {
            holder.itemView.time.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mWinnerList.size();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        WinnerItemBinding itemView;

        private ViewHolderItem(WinnerItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

