package com.flashbid.flashcoinapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.R;
import com.flashbid.flashcoinapp.databinding.ResultItemBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.Results;
import com.flashbid.flashcoinapp.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolderItem> {
    private final Context mContext;
    private List<Results> mList;
    private final HomeListener mCallback;

    public ResultAdapter(Context mContext, List<Results> mList, HomeListener mCallback) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ResultItemBinding mBindingItem = ResultItemBinding.inflate
                (LayoutInflater.from(parent.getContext()), parent, false);
        return new ResultAdapter.ViewHolderItem(mBindingItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderItem holder, int position) {
        final Results result = mList.get(position);
        final String type = result.getType();
        String title;
        if (type.equalsIgnoreCase(Results.LOTTERY)) {
            title = mContext.getString(R.string.lottery);
        } else if (type.equalsIgnoreCase(Results.TRIVIALITIES)) {
            title = mContext.getString(R.string.trivialities);
        } else if (type.equalsIgnoreCase(Results.AUCTION)) {
            title = mContext.getString(R.string.auction);
        } else {
            title = mContext.getString(R.string.special_auction);
        }
        holder.itemView.title.setText(title);
        holder.itemView.message.setText(result.getMessage());

        if (result.getWinners().size() > 0) {
            holder.itemView.moreItemIcon.setVisibility(View.VISIBLE);
            setAdapter(holder, result);
            holder.itemView.moreItemIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleLayout(holder,result);
                }
            });
            holder.itemView.resultLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleLayout(holder,result);
                }
            });
        } else {
            holder.itemView.moreItemIcon.setVisibility(View.GONE);
            holder.itemView.recyclerView.setAdapter(null);
            holder.itemView.moreItemIcon.setOnClickListener(null);
            holder.itemView.resultLayout.setOnClickListener(null);
        }

        if(result.isWinnerListVisible()) {
            holder.itemView.recyclerView.setVisibility(View.VISIBLE);
            holder.itemView.moreItemIcon.setRotation(180);
        } else {
            holder.itemView.recyclerView.setVisibility(View.GONE);
            holder.itemView.moreItemIcon.setRotation(0);
        }
        if (type.equalsIgnoreCase(Results.TRIVIALITIES)) {
            holder.itemView.icon.setImageResource(R.mipmap.group_4);
        } else {
            if (!TextUtils.isEmpty(result.getImage())) {
                Picasso.get().load(result.getImage()).placeholder(R.mipmap.profile).into(holder.itemView.icon);
            } else {
                holder.itemView.icon.setImageResource(R.mipmap.profile);
            }
        }
        holder.itemView.updateTime.setText(result.getUpdatedAt());

    }


    private void toggleLayout(ViewHolderItem holder, Results result) {
        if (result.isWinnerListVisible()) {
            holder.itemView.recyclerView.setVisibility(View.GONE);
        } else {
            holder.itemView.recyclerView.setVisibility(View.VISIBLE);
        }
        result.setWinnerListVisible(!result.isWinnerListVisible());
        notifyDataSetChanged();
    }

    private void setAdapter(ViewHolderItem holder, Results result) {
        WinnerAdapter mAdapter = new WinnerAdapter(mContext, result.getWinners(), mCallback);
        holder.itemView.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        holder.itemView.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void setList(List<Results> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        ResultItemBinding itemView;

        private ViewHolderItem(ResultItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}