package com.flashbid.flashcoinapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.flashbid.flashcoinapp.databinding.FidelityItemBinding;
import com.flashbid.flashcoinapp.interfaces.HomeListener;
import com.flashbid.flashcoinapp.models.Loyalty;


import java.util.List;


public class FidelityAdapter extends RecyclerView.Adapter<FidelityAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Loyalty> mList;
    private HomeListener mCallback;

    public FidelityAdapter(Context mContext, List<Loyalty> mList, HomeListener mCallback) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FidelityItemBinding mBinding = FidelityItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderItem holder,final int position) {
        final Loyalty loyalty = mList.get(position);
        holder.itemView.titleInitial.setText(""+Character.toUpperCase(loyalty.getCompanyName().charAt(0)));
        holder.itemView.title.setText(loyalty.getCompanyName());
        holder.itemView.points.setText(""+loyalty.getPoints());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Loyalty> items) {
        mList = items;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        FidelityItemBinding itemView;
        private ViewHolderItem(FidelityItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

